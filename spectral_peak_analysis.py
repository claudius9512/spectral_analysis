from spectral_analysis import combine_and_write_to_log
from spectral_analysis import get_file_basenames
from spectral_analysis import measurement_control_variable_value
from spectral_analysis import parsing
from spectral_analysis import select_data_within_xrange
from spectral_analysis import Tee

from data_plotter import cmap_map

from typing import Any, TextIO

import io
import json
import lmfit
import numpy as np

import scipy.signal as scs

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import matplotlib.ticker as tck

import multiprocessing as mp

import os
import sys
import time

if sys.platform == 'darwin':
    mpl.use('Qt5Agg')


class DataSet():
    # https://stackoverflow.com/questions/12101958/how-to-keep-track-of-class-instances
    # https://stackoverflow.com/questions/9056955/what-does-super-mean-in-new
    def __init__(self, file_basenames, general_parameters):
        self.file_basenames = file_basenames
        self.general_parameters = general_parameters
        self.LK_RT_save_dir = ''
        self.dataset = {}
        self.B_avg = None
        self.sample_name = ''
        self.peak = {}
        self.LK_RT_data_fpath = ''
        self.LK_RT_data = np.array([[], []])
        self.frequency_info = {}
        self.LK_RT_MCV_bounds = []
        self.fit_data_select_state = ''
        self.LK_RT_MCV_excluded = None

    def load_data(self):
        print('Loading files...')
        for file_basename in self.file_basenames:
            file_path = self.general_parameters['input_fresource_dir'] + os.sep + file_basename

            if self.sample_name == '':
                # Spectral Analysis files are always of the form <...>.<sample_name>.<range_indicator>.<extension>
                self.sample_name = file_basename.split('.')[-3]

            if self.B_avg is None:
                self.B_avg = calculate_B_avg(fpath=file_path,
                                             row=self.general_parameters['input_comments_line_no'],
                                             delimiter=self.general_parameters['input_delim'],
                                             comment_str=self.general_parameters['input_comment_str']
                                             )

            x, y = np.loadtxt(file_path,
                              delimiter=self.general_parameters['input_delim'],
                              skiprows=self.general_parameters['input_skiprows'],
                              unpack=True
                              )

            data_label = create_label(file_path,
                                      row=self.general_parameters['input_comments_line_no'],
                                      delimiter=self.general_parameters['input_delim'],
                                      comment_str=self.general_parameters['input_comment_str'],
                                      var=self.general_parameters['input_MCV_name']
                                      )

            self.dataset.update({data_label: (x, y)})

        print(f'Sample:\t{self.sample_name}')
        print(f'Calculated average field:\t{self.B_avg} T\n\n')

    def clear_data(self):
        self.dataset = {}

    def clear_peak(self):
        self.peak = {}

    def set_peak(self):
        self.clear_peak()

        peak_bounds: list[float] = self.set_bounds()
        peak_marker: float = self.set_last_visible_peak_marker()

        last_visible_peak_position = None

        for data_label, data in self.dataset.items():
            x_bounded, y_bounded = select_data_within_xrange(x=data[0],
                                                             y=data[1],
                                                             MIN=peak_bounds[0],
                                                             MAX=peak_bounds[1])
            if float(data_label.replace(f' {self.general_parameters["input_MCV_units"]}', '')) <= peak_marker:
                # WATCH OUT: this is a tuple containing arrays
                array_index_of_peak = scs.argrelmax(y_bounded)
                assert len(array_index_of_peak) == 1 and len(array_index_of_peak[0]) == 1

                x_peak, y_peak = x_bounded[array_index_of_peak[0][0]], y_bounded[array_index_of_peak[0][0]]

                if float(data_label.replace(f' {self.general_parameters["input_MCV_units"]}', '')) == peak_marker:
                    last_visible_peak_position = x_peak
            else:
                assert last_visible_peak_position is not None

                # https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
                # WATCH OUT: this is an INTEGER
                array_index_of_peak = np.abs(x_bounded - last_visible_peak_position).argmin()

                x_peak, y_peak = x_bounded[array_index_of_peak], y_bounded[array_index_of_peak]

            self.peak.update({data_label: (x_peak, y_peak)})

    @staticmethod
    def set_bounds(check_list=None):
        # Start with empty list
        bounds = []

        # Loop structure for value selection
        while len(bounds) < 2:
            # First enter lower bound, then enter upper bound
            if not bounds:
                message = 'lower bound:\t'
            else:
                message = 'upper bound:\t'
            try:
                user_input = input(message)
                if user_input == 'reset':
                    bounds = []
                    continue
                temp_bound = abs(float(user_input))
                if check_list:
                    if temp_bound not in check_list:
                        continue
                if bounds:
                    # Make sure that upper bound is larger than lower bound
                    if temp_bound <= bounds[0]:
                        continue
                bounds.append(temp_bound)
                print(temp_bound)
            # Add error handling
            except (KeyboardInterrupt, SystemExit):
                raise
            except ValueError:
                continue

        print(f'peak boundary: {bounds}')

        return bounds

    def set_last_visible_peak_marker(self):
        # Set marker that indicates the final data file (from low to high temperature) for which a peak is visible
        message = 'Enter MCV marking a data file for last visible peak:\t'

        marker = None

        MCVs = [float(key.replace(f' {self.general_parameters["input_MCV_units"]}', '')) for key in self.dataset.keys()]

        for MCV in MCVs:
            print(MCV)

        while marker not in MCVs:
            try:
                marker = float(input(message))
                if marker in MCVs:
                    print(marker)
                    break
                else:
                    continue
            except (KeyboardInterrupt, SystemExit):
                raise
            except ValueError:
                continue

        print(f'Peak last visible for T={marker} {self.general_parameters["input_MCV_units"]}')
        return marker

    def LK_RT_set_save_directory(self):

        SA_directory = self.general_parameters['input_fresource_dir']
        spectral_analysis_directory_name = SA_directory.split(os.sep)[-2]
        range_directory_name = SA_directory.split(os.sep)[-1]

        self.LK_RT_save_dir = SA_directory + os.sep + spectral_analysis_directory_name + '_' + range_directory_name + \
                              '_peaks'

        if not os.path.exists(self.LK_RT_save_dir):
            # create unique save directory
            os.makedirs(self.LK_RT_save_dir)
            print(f'Created directory {self.LK_RT_save_dir}')

    def LK_RT_extract_data(self):
        self.LK_RT_clear_data()
        self.LK_RT_clear_frequency_info()
        frequency = np.array([])

        for MCV_label, peak_coord in self.peak.items():
            self.LK_RT_data = np.append(self.LK_RT_data,
                                        [[float(MCV_label.replace(f' {self.general_parameters["input_MCV_units"]}', ''))],
                                         [peak_coord[1]]
                                         ],
                                        axis=1
                                        )
            frequency = np.append(frequency,
                                  peak_coord[0]
                                  )
        self.frequency_info.update({'f': frequency,
                                    'frequency_lowest_T': frequency[0],
                                    'mean': np.mean(frequency),
                                    'std': np.std(frequency)
                                    }
                                   )

        print('f=', np.mean(frequency), "+/-", np.std(frequency), 'T\n')

    def LK_RT_clear_data(self):
        self.LK_RT_data = np.array([[], []])

    def LK_RT_clear_frequency_info(self):
        self.frequency_info = {}

    def LK_RT_data_within_MCV_bounds(self):
        LK_RT_data_bounded = np.array([[], []])

        self.clear_MCV_bounds()
        self.set_MCV_bounds()

        x_LK_RT_bounded, y_LK_RT_bounded = select_data_within_xrange(x=self.LK_RT_data[0],
                                                                     y=self.LK_RT_data[1],
                                                                     MIN=self.LK_RT_MCV_bounds[0],
                                                                     MAX=self.LK_RT_MCV_bounds[1]
                                                                     )
        LK_RT_data_bounded = np.append(LK_RT_data_bounded,
                                       [x_LK_RT_bounded,
                                        y_LK_RT_bounded
                                        ],
                                       axis=1)

        return LK_RT_data_bounded

    def LK_RT_exclude_points_from_data(self):
        LK_RT_MCV_excluded = None
        LK_RT_data_without_excluded = np.array([[], []])

        MCVs = [float(key.replace(f' {self.general_parameters["input_MCV_units"]}', '')) for key in self.dataset.keys()]

        for MCV in MCVs:
            print(MCV)

        while True:
            user_input_exclude = input('Enter all MCVs you want to exclude separated by a comma:\n')
            try:
                LK_RT_MCV_excluded = [float(mcv.replace(' ', '')) for mcv in user_input_exclude.split(',')]
                for MCV_excluded in LK_RT_MCV_excluded:
                    if MCV_excluded not in MCVs:
                        print('Wrong value(s) entered.\n')
                        print(MCV_excluded)
                        continue
                print(user_input_exclude)
                break
            except ValueError:
                print('Could not convert entered value(s) to type float.\n')
                continue

        self.LK_RT_MCV_excluded = LK_RT_MCV_excluded
        print(LK_RT_MCV_excluded)
        temp_x = []
        temp_y = []
        for i in range(len(self.LK_RT_data[0])):
            if self.LK_RT_data[0][i] not in LK_RT_MCV_excluded:
                print(self.LK_RT_data[0][i])
                temp_x.append(self.LK_RT_data[0][i])
                temp_y.append(self.LK_RT_data[1][i])
        temp_x = np.asarray(temp_x)
        temp_y = np.asarray(temp_y)
        LK_RT_data_without_excluded = np.append(LK_RT_data_without_excluded,
                                                [temp_x,
                                                 temp_y,
                                                 ],
                                                axis=1
                                                )

        return LK_RT_data_without_excluded

    def set_MCV_bounds(self, active_max_bounds=False):
        MCVs = [float(key.replace(f' {self.general_parameters["input_MCV_units"]}', '')) for key in self.dataset.keys()]
        if not active_max_bounds:
            self.LK_RT_MCV_bounds = self.set_bounds(check_list=MCVs)
        else:
            self.LK_RT_MCV_bounds = [self.LK_RT_data[0][0],
                                     self.LK_RT_data[0][-1]
                                     ]

    def clear_MCV_bounds(self):
        self.LK_RT_MCV_bounds = []

    def LK_RT_set_fpath(self):
        self.LK_RT_data_fpath = self.LK_RT_save_dir + os.sep + os.path.basename(self.LK_RT_save_dir) + '.' + \
                                self.sample_name + '.' + f'f{self.frequency_info["frequency_lowest_T"]:.0f}T' + '.' + \
                                self.general_parameters['input_file_ext']

    def LK_RT_save_data(self, fpath):
        with open(fpath, 'w') as fp:
            fp.write(f'{self.general_parameters["input_comment_str"]} T\t'
                     'ampl\t'
                     'norm_ampl\t'
                     'f'
                     '\n'
                     )
            fp.write(f'{self.general_parameters["input_comment_str"]} K\t'
                     'arb. units\t'
                     'arb. units\t'
                     'T'
                     '\n'
                     )
            fp.write(f'{self.general_parameters["input_comment_str"]} f = {self.frequency_info["frequency_lowest_T"]}\t'
                     f'B_avg = {self.B_avg} T\t'
                     '\t'
                     f'<f> = {self.frequency_info["mean"]} +/- {self.frequency_info["std"]} T'
                     '\n'
                     )
            for i in range(len(self.LK_RT_data[0])):
                fp.write(f'{self.LK_RT_data[0][i]}\t'
                         f'{self.LK_RT_data[1][i]}\t'
                         f'{self.LK_RT_data[1][i] / max(self.LK_RT_data[1])}\t'
                         f'{self.frequency_info["f"][i]}'
                         '\n'
                         )


def save_fit_report(fit_report, save_dir, sample_name, freq_info, bounds, select_state, MCV_excluded,
                    general_parameters, ext='txt'):

    if select_state == 'FitAll' or select_state == 'FitIntvl':
        fname = sample_name + '.' + f'f{freq_info["frequency_lowest_T"]:.0f}T' + '.LK_RT_fit-report.' + \
                f'{select_state}_' + general_parameters["input_MCV_name"] + str(bounds[0]).replace('.', 'p') + '-' + \
                str(bounds[1]).replace('.', 'p') + general_parameters["input_MCV_units"]
    elif select_state == 'FitXcld':
        MCV_excluded_fname_part_ls = []

        for xcld in MCV_excluded:
            MCV_excluded_fname_part_ls.append(general_parameters["input_MCV_name"] + str(xcld).replace('.', 'p') + \
                                              general_parameters["input_MCV_units"])

        if len(MCV_excluded_fname_part_ls) > 1:
            MCV_excluded_fname_part = '_'.join(MCV_excluded_fname_part_ls)
        else:
            MCV_excluded_fname_part = MCV_excluded_fname_part_ls[0]

        fname = sample_name + '.' + f'f{freq_info["frequency_lowest_T"]:.0f}T' + '.LK_RT_fit-report.' + \
                f'{select_state}_' + MCV_excluded_fname_part
    else:
        print('Wrong data selection state for fit!')
        sys.exit(1)

    fpath = save_dir + os.sep + os.path.basename(save_dir) + '.' + fname + '.' + ext
    with open(fpath, 'w') as fp:
        fp.write(fit_report)


def create_label(fpath: str, row: int, delimiter: str, comment_str: str, var: str) -> str:

    with open(fpath, 'r') as fp:
        for i in range(0, row-1):
            next(fp)
        label = fp.readline().strip(comment_str).strip('\n').split(delimiter)[1].replace(var, '')

    return label


def calculate_B_avg(fpath: str, row: int, delimiter: str, comment_str: str) -> float:

    with open(fpath, 'r') as fp:
        for i in range(0, row-1):
            next(fp)
        B_interval = [float(B) for B in
                      fp.readline().strip(comment_str).split(delimiter)[0].strip('T').replace(' ', '').split('-')
                      ]

        B_avg = 2. / ((1./B_interval[0]) + (1./B_interval[1]))

    return B_avg


def user_io(*args):
    mssgs = args
    if len(mssgs) == 1:
        while True:
            user_input = input(mssgs[0])
            print(user_input)
            if user_input.lower().startswith('y'):
                return True, 'yes'
            elif user_input.lower().startswith('n'):
                return False, ''
            else:
                continue
    elif len(mssgs) == 2 and 'interval' in mssgs[0] and 'excluding' in mssgs[1]:
        while True:
            user_input = input(mssgs[0] + '\nOR\n\n' + mssgs[1] + '\nOR\n\n' + 'Use all data points for fit? (i/e/a)\n')
            print(user_input)
            if user_input.lower().startswith('i'):
                return True, 'interval'
            elif user_input.lower().startswith('e'):
                return True, 'exclude'
            elif user_input.lower().startswith('a'):
                return False, ''
            else:
                continue
    else:
        print('This type of asking user for input is not supported')
        sys.exit(1)


def LK_RT(x, A0, mc, B_avg):
    """Lifshitz-Kosevich temperature reduction factor fit"""
    return A0 * (14.69*mc*x/B_avg) / np.sinh(14.69*mc*x/B_avg)


def plot_LK_RT_data(full_dataset=None, fitted_dataset=None, plot_parameters=None, freq_info=None, fit_best_params=None,
                    fit=None, sample_name=None, bounds=None):

    fig = plt.figure()
    ax = fig.add_subplot()

    if fit is None:
        ax.semilogx(full_dataset[0], full_dataset[1], 'bo', label=f'{freq_info["frequency_lowest_T"]:.0f} T')

        ax.set_xlabel('$T$ (K)')
        ax.set_ylabel('$A_{FFT}$ (arb. units)')
    else:
        assert fitted_dataset is not None
        x_fit = np.linspace(0.1, fitted_dataset[0][-1] + 100.0, num=1000)
        y_fit = LK_RT(x=x_fit,
                      A0=fit_best_params['A0'],
                      mc=fit_best_params['mc'],
                      B_avg=fit_best_params['B_avg']
                      )
        if fitted_dataset[0].size == full_dataset[0].size:
            ax.semilogx(fitted_dataset[0], fitted_dataset[1], 'bo', label=f'{freq_info["frequency_lowest_T"]:.0f}T')

            ax.semilogx(x_fit, y_fit, 'r-', label='LK $R_{T}$ fit')
        else:
            ax.semilogx(full_dataset[0], full_dataset[1], 'bo', label=f'{freq_info["frequency_lowest_T"]:.0f}T')
            ax.semilogx(fitted_dataset[0], fitted_dataset[1], 'ro', markersize=3, label=f'used for fit')
            ax.semilogx(x_fit, y_fit, 'g-.', label='LK $R_{T}$ fit extrapolated')
            ax.semilogx(fitted_dataset[0], fit, 'r--', label='LK $R_{T}$ fit')

        ax.semilogx([], [], ' ', label='$m_{c}$ $[m_{0}]$=' + f'{fit_best_params["mc"]:.3f}')
        ax.semilogx([], [], ' ', label='$A_{0}$=' + f'{fit_best_params["A0"]:.3f}')
        ax.semilogx([], [], ' ', label='$B_{avg}$ [T]=' + f'{fit_best_params["B_avg"]:.3f} (fixed)')
        ax.semilogx([], [], ' ', label=r'$R_{T} = A_{0} \frac{14.69m_{c}T/B_{avg}}{\sinh{(14.69 m_{c}T/B_{avg})}}$')

        # set plot boundaries
        ax.set_aspect('auto')
        ax.set_xbound(lower=0.1,
                      upper=full_dataset[0][-1] + 5.0
                      )

        # set ticks
        ax.minorticks_on()
        ax.tick_params(axis='both',
                       which='both',
                       direction=plot_parameters['plot_ticks_dir'],
                       bottom=True,
                       top=True,
                       left=True,
                       right=True)

        # set labels
        ax.set_xlabel('$T$ (K)')
        ax.set_ylabel('$A_{FFT}^{norm}$')

        # set title
        ax.set_title(f'{sample_name}: {freq_info["frequency_lowest_T"]:.0f}T' + ' - LK $R_{T}$ fit ' +
                     f'[$T$={bounds[0]} - {bounds[1]}K]')

    # make legend
    leg = ax.legend()
    # making the legend draggable gives continuous KeyError due to click event also being linked to the legend
    # handles
    leg.set_draggable(state=plot_parameters['plot_legend_draggable'])

    plt.show()


def plot_spectrum(plot_parameters, data_set, freq_info=None):
    fig = plt.figure()
    ax = fig.add_subplot()

    # get colormap
    cmap_colors = cmap_map(lambda _x: _x * plot_parameters["plot_cmap_tuner"],
                           plot_parameters["plot_cmap_name"],
                           plot_parameters["plot_cmap_tuner"]
                           )
    cmap = cmap_colors(np.linspace(0, 1, len(data_set.dataset.keys())))
    ax.set_prop_cycle('color', cmap)

    plotted_lines = []
    for data_label, data in data_set.dataset.items():

        print(f'plotting {data_label}')
        line, = ax.plot(data[0], data[1], label=data_label)
        plotted_lines.append(line)

        # create legend
        if plot_parameters['plot_reverse']:
            leg_handles, labels = ax.get_legend_handles_labels()
            leg = ax.legend(reversed(leg_handles),
                            reversed(labels),
                            ncol=plot_parameters['plot_legend_ncol']
                            )
            plotted_lines = plotted_lines[::-1]
        else:
            leg = ax.legend(ncol=plot_parameters['plot_legend_ncol'])
        for leg_handle in leg.legendHandles:
            leg_handle.set_linewidth(plot_parameters['plot_legend_linewidth'])
        # making the legend draggable gives continuous KeyError due to click event also being linked to the legend
        # handles
        leg.set_draggable(state=plot_parameters['plot_legend_draggable'])

    if data_set.peak:
        for peak_coord in data_set.peak.values():
            peak_line = ax.plot(peak_coord[0], peak_coord[1], 'rx')

    # for visible/non-visible lines
    lined = {}  # Will map legend lines to original lines.
    for legline, origline in zip(leg.get_lines(), plotted_lines):
        legline.set_picker(True)  # Enable picking on the legend line.
        lined[legline] = origline

    def on_pick(event):
        # https://matplotlib.org/stable/gallery/event_handling/legend_picking.html
        # On the pick event, find the original line corresponding to the legend
        # proxy line, and toggle its visibility.
        legline = event.artist
        origline = lined[legline]
        visible = not origline.get_visible()
        origline.set_visible(visible)
        # Change the alpha on the line in the legend so we can see what lines
        # have been toggled.
        legline.set_alpha(1.0 if visible else 0.2)
        fig.canvas.draw()

    fig.canvas.mpl_connect('pick_event', on_pick)

    # set plot boundaries
    ax.set_aspect('auto')
    ax.set_xbound(lower=plot_parameters['plot_xlb'], upper=plot_parameters['plot_xub'])

    # set ticks
    ax.minorticks_on()
    ax.tick_params(axis='both',
                   which='both',
                   direction=plot_parameters['plot_ticks_dir'],
                   bottom=True,
                   top=True,
                   left=True,
                   right=True)
    xminor_locator = tck.AutoMinorLocator(2)
    yminor_locator = tck.AutoMinorLocator(2)
    ax.xaxis.set_minor_locator(xminor_locator)
    ax.yaxis.set_minor_locator(yminor_locator)

    # set labels
    ax.set_xlabel('$f$ (T)')
    ax.set_ylabel('$A_{FFT}$ (arb. units)')

    # set title
    if not data_set.peak:
        ax.set_title(f'{data_set.sample_name}')
    else:
        ax.set_title(f'{data_set.sample_name}: {freq_info["frequency_lowest_T"]:.0f}T')

    plt.show()


if __name__ == '__main__':

    # get absolute path to json file (stores all user input) from parsed argument
    json_fp = parsing().json_filepath

    # get boolean indicating whether standard system I/O rerouting will be disabled
    sys_ioe_flag = True

    # load content from json file into a dictionary
    with open(json_fp) as json_content:
        parameters = json.load(json_content)

    plot_params = parameters['plot_params']
    general_params = parameters['general_params']

  #   plot_params = {"plot_cmap_name": "jet",
  # "plot_cmap_tuner": 0.725,
  #                  "plot_ticks_dir": "in",
  #       "plot_reverse": False,
  #                      "plot_legend_ncol": 1,
  #                      "plot_legend_linewidth": 2.5,
  #                      "plot_legend_draggable": True,
  #                  "plot_xlb": 0.0,
  #                  "plot_xub": 5000,
  #                      }
  #
  #   general_params = {"input_fresource_dir": "/Users/cmuller/Resources/Data/2018_Apr_Cell2/HfSiS_Tdep_CorrData/SA_FFT6/r1",
  #                         "input_files_to_analyze": [],
  #                         "input_delim": "\t",
  #                         "input_file_ext": "dat",
  #                         "input_comment_str": "#",
  #                         "input_skiprows": 0,
  #                         "input_use_columns_x": 0,
  #                         "input_use_columns_y": 1,
  #                         "input_var_names_line_no": 1,
  #                         "input_comments_line_no": 2,
  #                         "input_data_type": "float",
  #                         "input_MCV_name": "T",
  #                         "input_MCV_units": "K",
  #                         "LK_RT_normalize_by_max": True
  #                         }

    # -------------------------------------------------------------------------
    #
    # 1st PART
    #
    # -------------------------------------------------------------------------
    #
    #
    #
    #############################
    # CREATE GENERAL LOG STREAM #
    #############################

    # save a copy of the standard sys.stdout/sys.stderr
    sys_stdout: TextIO = sys.stdout

    # define a separate text stream for logging general output
    general_log_stream_stdout = io.StringIO()

    # tee sys.stdout
    # NOTE: type ignore due to
    sys.stdout = Tee(user_ioe=general_log_stream_stdout, sys_active=sys_ioe_flag, sys_ioe=sys_stdout)  # type: ignore

    #
    #
    #
    # -------------------------------------------------------------------------
    print('\nSetting up dataset')
    # get list of base name of each input file
    input_fbasenames: list[str] = get_file_basenames(rsrc_dir=general_params['input_fresource_dir'],
                                                     specific=general_params['input_files_to_analyze'],
                                                     ext=general_params['input_file_ext'],
                                                     exclude=True
                                                     )

    # sort file base names according to value of the measurement variable that is standard included in file  base name
    input_fbasenames = sorted(input_fbasenames,
                              key=measurement_control_variable_value
                              )
    dataset = DataSet(general_parameters=general_params,
                      file_basenames=input_fbasenames
                      )
    dataset.load_data()
    plot_params.update({'sample_name': dataset.sample_name})

    plot_spectrum_on_another_core = mp.Process(target=plot_spectrum,
                                               args=(plot_params,
                                                     dataset
                                                     )
                                               )
    plot_spectrum_on_another_core.start()
    time.sleep(2)

    # -------------------------------------------------------------------------
    #
    # 2nd PART
    #

    LK_RT_model = lmfit.Model(LK_RT)
    LK_RT_model_params = LK_RT_model.make_params()
    LK_RT_model_params.add('A0',
                           value=1.0,
                           vary=True
                           )
    LK_RT_model_params.add('mc',
                           value=1.0,
                           min=0.0,
                           max=100.0,
                           vary=True
                           )
    LK_RT_model_params.add('B_avg',
                           value=dataset.B_avg,
                           vary=False
                           )

    # -------------------------------------------------------------------------
    #
    # 3rd PART
    #
    dataset.LK_RT_set_save_directory()
    user_continue = True

    while user_continue:
        dataset.set_peak()

        dataset.LK_RT_extract_data()
        data_LK_RT = dataset.LK_RT_data
        frequency_info = dataset.frequency_info

        plot_spectrum_and_peak_on_another_core = mp.Process(target=plot_spectrum,
                                                            args=(plot_params,
                                                                  dataset
                                                                  ),
                                                            kwargs={'freq_info': frequency_info
                                                                    }
                                                            )
        plot_spectrum_and_peak_on_another_core.start()
        time.sleep(2)
        dataset.LK_RT_set_fpath()
        LK_RT_save_data_fpath = dataset.LK_RT_data_fpath
        if not os.path.exists(LK_RT_save_data_fpath):
            messages = ['Save frequency vs temperature data? (y/n)\n']
            bool_save_LK_RT_data, _ = user_io(*messages)
        else:
            bool_save_LK_RT_data = False
        if bool_save_LK_RT_data:
            print(f'Saving frequency vs temperature data for peak f={frequency_info["frequency_lowest_T"]:.0f}T')
            dataset.LK_RT_save_data(LK_RT_save_data_fpath)
        else:
            print(f'Not saving frequency vs temperature data for peak f={frequency_info["frequency_lowest_T"]:.0f}T')

        # normalize amplitudes of LK data
        if general_params['LK_RT_normalize_by_max']:
            data_LK_RT[1] = data_LK_RT[1]/max(data_LK_RT[1])

        plot_LK_RT_data_on_another_core = mp.Process(target=plot_LK_RT_data,
                                                     kwargs={'full_dataset': data_LK_RT,
                                                             'plot_parameters': plot_params,
                                                             'freq_info': frequency_info
                                                             }
                                                     )
        plot_LK_RT_data_on_another_core.start()
        time.sleep(1)

        messages = ['Fit only within certain MCV interval?\n',
                    'Fit by excluding certain MCVs?\n']
        bool_fit_select, fit_select_state = user_io(*messages)

        if bool_fit_select and fit_select_state == 'interval':
            data_LK_RT = dataset.LK_RT_data_within_MCV_bounds()
            dataset.fit_data_select_state = 'FitIntvl'
            print(f'Will only fit in interval {dataset.LK_RT_MCV_bounds[0]} - {dataset.LK_RT_MCV_bounds[1]} K')
        elif bool_fit_select and fit_select_state == 'exclude':
            data_LK_RT = dataset.LK_RT_exclude_points_from_data()
            dataset.LK_RT_MCV_bounds = [data_LK_RT[0][0], data_LK_RT[0][-1]]
            dataset.fit_data_select_state = 'FitXcld'
            print(f'Will exclude the data points at {general_params["input_MCV_name"]}={dataset.LK_RT_MCV_excluded}'
                  f'{general_params["input_MCV_units"]}')
        else:
            dataset.set_MCV_bounds(active_max_bounds=True)
            dataset.fit_data_select_state = fit_select_state
            dataset.fit_data_select_state = 'FitAll'
            print('Will fit data over complete temperature interval and include all data points')

        LK_RT_fit = LK_RT_model.fit(data_LK_RT[1],
                                    LK_RT_model_params,
                                    x=data_LK_RT[0]
                                    )
        print(LK_RT_fit.fit_report())

        plot_LK_RT_data_fit_on_another_core = mp.Process(target=plot_LK_RT_data,
                                                         kwargs={'full_dataset': dataset.LK_RT_data,
                                                                 'fitted_dataset': data_LK_RT,
                                                                 'plot_parameters': plot_params,
                                                                 'freq_info': frequency_info,
                                                                 'fit_best_params': LK_RT_fit.best_values,
                                                                 'fit': LK_RT_fit.best_fit,
                                                                 'sample_name': dataset.sample_name,
                                                                 'bounds': dataset.LK_RT_MCV_bounds
                                                                 }
                                                         )
        plot_LK_RT_data_fit_on_another_core.start()
        time.sleep(2)

        messages = ['Save LK RT fit report? (y/n)\n']
        bool_save_fit_report, _ = user_io(*messages)
        if bool_save_fit_report:
            print('Saving fit report.')
            save_fit_report(fit_report=LK_RT_fit.fit_report(),
                            save_dir=dataset.LK_RT_save_dir,
                            sample_name=dataset.sample_name,
                            freq_info=frequency_info,
                            bounds=dataset.LK_RT_MCV_bounds,
                            select_state=dataset.fit_data_select_state,
                            MCV_excluded=dataset.LK_RT_MCV_excluded,
                            general_parameters=general_params
                            )
        else:
            print('Not saving fit report.')

        messages = ['Continue? (y/n)\n']
        bool_continue_peak_analysis, _ = user_io(*messages)
        if bool_continue_peak_analysis:
            user_continue = True
        else:
            user_continue = False

        # clean up
        plot_spectrum_and_peak_on_another_core.join()
        plot_LK_RT_data_on_another_core.join()
        plot_LK_RT_data_fit_on_another_core.join()

    # clean up
    plot_spectrum_on_another_core.join()

    print('finish')

    combine_and_write_to_log(general_log_stream_stdout,
                             dir_path=dataset.LK_RT_save_dir
                             )

    # -------------------------------------------------------------------------
    #
    #
    #
    ###############################
    # RESTORE DEFAULT I/O STREAM  #
    ###############################

    # restore original sys.stdout
    sys.stdout = sys_stdout

    #
    #
    #
    # -------------------------------------------------------------------------
