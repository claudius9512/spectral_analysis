import argparse as argp

import matplotlib as mpl  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import matplotlib.ticker as tck

import itertools

import json

import glob  # type: ignore
import numpy as np
import operator
import os
import re
import sys

from typing import Any, Match, Optional

from spectral_analysis import create_file_info
from spectral_analysis import get_file_basenames
from spectral_analysis import load_data
from spectral_analysis import measurement_control_variable_value
from spectral_analysis import validate_filepath

#
# def check_plot_derivative_order(calculate: dict[str, bool], exclude: dict[str, bool], d_order: str = None) -> str:
#     """
#     function that checks whether user entered derivative order is legit and not excluded.
#     NOTE: there are only three available derivative orders d0, d1, d2 indicating the 0th, 1st, and 2nd derivative, resp.
#
#     :param calculate: dictionary of string-boolean (key-value) pairs indicating which derivative order(s) are
#                       calculated during interpolation.
#     :param exclude: dictionary storing which derivative orders need to be excluded from calculating
#     :param d_order: string indicating the derivative order user wants to be plotted
#     :return: (possibly altered) string indicating the derivative order depending on user input being legit or excluded
#     """
#     # check that all needed user inputs are available
#     if not d_order:
#         print('ERROR: activate showing plots or select legit derivative order that will be plotted!')
#         sys.exit(1)
#
#     # give access to global variable containing all possible derivative orders
#     global derivative_orders
#     # default 0th derivative order that should be always possible, if not excluded
#     possible_deriv: tuple[str, ...] = derivative_orders[:1]
#     # add only 1st derivative order to selection if d1 is active
#     if calculate['d1']:
#         possible_deriv = derivative_orders[:2]
#     # add 1st and 2nd derivative orders to selection if d2 is active
#     elif calculate['d1']:
#         possible_deriv = derivative_orders[:3]
#
#     # if string indicating derivative order to be plotted is legit and not excluded, return it
#     if d_order in [elem for elem in possible_deriv if not exclude[elem]]:
#         return d_order
#     # else: if user input of d_order is not legit or excluded, define d_order by one of the remaining possible options
#     else:
#         print(f'WARNING: derivative order {d_order} is not allowed or excluded!')
#         # if highest available derivative order (d2) is excluded, return next lower derivative order d1
#         if exclude[possible_deriv[-1]]:
#             print(f'Setting it to second highest allowed order {possible_deriv[-2]}')
#             return possible_deriv[-2]
#         # else: return highest derivative order d2, if it is not excluded
#         else:
#             print(f'Setting it to highest allowed order {possible_deriv[-1]}')
#             return possible_deriv[-1]


def cmap_map(function, cmap_name, tuner):
    """ Applies function (which should operate on vectors of shape 3: [r, g, b]), on colormap cmap.
    This routine will break any discontinuous points in a colormap.
    LINK: https://scipy-cookbook.readthedocs.io/items/Matplotlib_ColormapTransformations.html
    """
    # https://stackoverflow.com/questions/3279560/reverse-colormap-in-matplotlib
    cmap = mpl.cm.get_cmap(cmap_name)
    try:
        cdict = cmap._segmentdata
        step_dict = {}
        # get the list of points where the segments start or end
        for key in ('red', 'green', 'blue'):
            step_dict[key] = list(map(lambda x: x[0], cdict[key]))
        step_list = sum(step_dict.values(), [])
        step_list = np.array(list(set(step_list)))
        # compute the LUT, and apply the function to the LUT
        reduced_cmap = lambda _step: np.array(cmap(_step)[0:3])
        old_LUT = np.array(list(map(reduced_cmap, step_list)))
        new_LUT = np.array(list(map(function, old_LUT)))
        # try to make a minimal segment definition of the new LUT
        cdict = {}
        for i, key in enumerate(['red', 'green', 'blue']):
            this_cdict = {}
            for j, step in enumerate(step_list):
                if step in step_dict[key]:
                    this_cdict[step] = new_LUT[j, i]
                elif new_LUT[j, i] != old_LUT[j, i]:
                    this_cdict[step] = new_LUT[j, i]
            colorvector = list(map(lambda x: x + (x[1], ), this_cdict.items()))
            colorvector.sort()
            cdict[key] = colorvector

        return mpl.colors.LinearSegmentedColormap('colormap', cdict, 1024)

    except AttributeError:
        # https://stackoverflow.com/questions/43384448/modifying-viridis-colormap-replacing-some-colors-in-matplotlib
        cmaplist = [cmap(i) for i in range(cmap.N)]
        cmaplist_new = []
        for cmaplist_elem in cmaplist:
            new_color = ()
            for i in range(0, len(cmaplist_elem)):
                new_color += (cmaplist_elem[i]*tuner,)
            cmaplist_new.append(new_color)
        return mpl.colors.LinearSegmentedColormap.from_list('colormap', cmaplist_new, cmap.N)


def get_directories(parent_dir: str) -> list[str]:
    """
    function returning list of absolute paths that stand for each child directory present in the given parent directory

    :param parent_dir: string indicating the absolute path of the parent directory
    :return: list of strings with each string being the absolute path of each child directory in the given parent
             directory
    """
    # check that a parent directory is given
    if not parent_dir:
        print('ERROR: cannot give child directories for no specified parent directory!')
        sys.exit(1)

    # return list of child directories
    # https://stackoverflow.com/questions/800197/how-to-get-all-of-the-immediate-subdirectories-in-python
    return [parent_dir + os.sep + name for name in os.listdir(parent_dir)
            if os.path.isdir(os.path.join(parent_dir, name))
            ]


def numsort_dirs(dir_path: str) -> int:
    """
    function that returns the unique integer identifying the input directory

    :param dir_path: string indicating the absolute path of the input directory
    :return: integer that uniquely identifies the input directory
    """

    # get the base name of the input directory
    # NOTE: input directory base names should consist out of capital or lower letters only!
    dir_basename: str = os.path.basename(dir_path)

    # define pattern for regex expression search
    string_pattern: str = r'([A-Z]+(_)?[A-Z]+|[a-z])'
    string_match: Optional[Match[str]] = re.search(string_pattern, dir_basename)
    assert string_match is not None

    # get the uniquely identifying integer by replacing the letter part of the input directory base name with nothing
    dir_basename_no = int(dir_basename.replace(string_match.group(), ''))

    # return uniquely identifying integer
    return dir_basename_no

# def main_old():
#
#     # choose which derivative order to plot
#     plot_deriv_orders: list[str] = ['d1']
#
#     # booleans controlling whether to show plots wrt var [temperature, angle, ...] or range dependence
#     # NOTE: (var, rng)_dep_bool are not used if show_bool is False
#     var_dep_bool: bool = True
#     rng_dep_bool: bool = False
#     show_bool=False
#     #
#     #
#     #
#     # -------------------------------------------------------------------------
#
#     fresource_dir = ''
#
#     # -------------------------------------------------------------------------
#     #
#     #
#     #
#     #############################################
#     # Perform SANITY CHECKS for PLOT PARAMETERS #
#     #############################################
#
#     if show_bool:
#         print('Checking plot parameters...')
#
#         # plot_deriv_order = check_plot_derivative_order(calculate=calculate_deriv_orders,
#         #                                                exclude=exclude_deriv_orders,
#         #                                                d_order=plot_deriv_order
#         #                                                )
#
#     #
#     #
#     #
#     # -------------------------------------------------------------------------
#
#     # show calculated spectra of MESA no. <save_dir_no>
#     # TODO: add Flag to only show specific MESA/FFT outputs, add Flag to only show specific range + measurement variable
#     #  combination, make dedicated plotter
#     if show_bool:
#         plot_save_dirs = get_directories(fresource_dir)
#
#         if not plot_save_dirs:
#             print('ERROR: perform MESA first!')
#             sys.exit(1)
#
#         for plot_save_dir in sorted(plot_save_dirs, key=numsort_dirs):
#             rng_dirs = get_directories(plot_save_dir)
#
#             if var_dep_bool:
#                 for rng_dir in sorted(rng_dirs, key=numsort_dirs):
#                     pass
#                     # show_spectra_var_dep(pdir_basename=os.path.basename(plot_save_dir),
#                     #                      fdir=rng_dir,
#                     #                      d_order=plot_deriv_order,
#                     #                      units=MCV_units
#                     #                      )
#
#             if rng_dep_bool:
#                 print('Not implemented, yet....')
#                 pass
#     return 0


def parsing():
    """
    function handling argument parsing

    :return: Namespace containing all parsed arguments
    """

    # https://stackoverflow.com/questions/59178961/how-to-get-file-path-from-user-to-then-pass-into-functions-argparse
    # https://www.golinuxcloud.com/python-argparse/
    # https://docs.python.org/dev/library/argparse.html#the-add-argument-method

    # create parser
    parser = argp.ArgumentParser()

    # add file path as positional argument
    parser.add_argument('json_filepath',
                        help='Absolute path to .json file where all input parameters are declared',
                        type=validate_filepath,
                        metavar='JSON FILEPATH'
                        )

    # return arguments
    return parser.parse_args()


def main(settings: dict[str, Any]) -> int:

    # _________________________________________________
    # get settings from json file
    input_method: str = settings['input_method']  # original, MCV, range, compare_SA

    input_SA_range: Optional[str] = settings['input_SA_range']

    input_SA_dirs: Optional[list[str]] = settings['input_SA_dirs']

    input_main_dir: str = settings['input_main_directory']

    input_derivative: str = settings['input_derivative']

    input_files_to_plot: list[str] = settings['input_files_to_plot']
    input_reverse: bool = settings['input_reverse']

    input_delim: str = settings['input_delim']
    input_file_ext: str = settings['input_file_ext']
    input_skiprows: int = settings['input_skiprows']
    input_use_columns: tuple[int, int] = (settings['input_use_columns_x'],
                                          settings['input_use_columns_y']
                                          )
    input_var_names_line_no: Optional[int] = settings['input_var_names_line_no']

    input_MCV_name: str = settings['input_MCV_name']
    input_MCV_units: str = settings['input_MCV_units']

    input_subtract_first_value: bool = settings['input_subtract_first_value']
    input_normalize_by_mean: bool = settings['input_normalize_by_mean']

    plot_cmap_name: str = settings['plot_cmap_name']
    plot_cmap_tuner: float = settings['plot_cmap_tuner']
    plot_rcParams: dict[str, Any] = settings['plot_rcParams']
    plot_ticks_dir: str = settings['plot_ticks_dir']
    plot_legend_linewidth: int = settings['plot_legend_linewidth']
    plot_legend_draggable: bool = settings['plot_legend_draggable']
    plot_legend_ncol: int = settings['plot_legend_ncol']
    plot_xlb: Optional[float] = settings['plot_xlb']
    plot_xub: Optional[float] = settings['plot_xub']
    plot_ylb: Optional[float] = settings['plot_ylb']
    plot_yub: Optional[float] = settings['plot_yub']
    plot_offset: Optional[float] = settings['plot_offset']
    plot_cos_scale: bool = settings['plot_cos_scale']
    plot_only_theory: bool = settings['plot_only_theory']
    plot_scatter: bool = settings['plot_scatter']
    plot_scatter_size: float = settings['plot_scatter_size']
    plot_scatter_vmin_threshold: float = settings['plot_scatter_vmin_threshold']
    plot_scatter_vmax_scaled: float = settings['plot_scatter_vmax_scaled']
    plot_scatter_marker: str = settings['plot_scatter_marker']
    plot_scatter_alpha: float = settings['plot_scatter_alpha']
    plot_scatter_theory: bool = settings['plot_scatter_theory']
    plot_scatter_DFTTony: bool = settings['plot_scatter_DFTTony']
    plot_scatter_DFT_ls_fpaths: list[str] = settings['plot_scatter_DFT_ls_fpaths']
    plot_scatter_DFT_ls_colors: list[str] = settings['plot_scatter_DFT_ls_colors']

    export_active: bool = settings['export_active']
    export_dir: str = settings['export_dir']
    export_only: bool = settings['export_only']

    # _________________________________________________
    # set the color map
    if not plot_scatter:
        cmap_colors = cmap_map(lambda _x: _x * plot_cmap_tuner, plot_cmap_name, plot_cmap_tuner)

    # _________________________________________________
    # set plot parameters
    for k, v in plot_rcParams.items():
        if v is not None:
            plt.rcParams[k] = v

    # _________________________________________________
    # get the right input directory
    input_data_dir: str = input_main_dir
    if input_method == 'original':
        pass
    elif input_method == 'MCV':
        input_data_dir = input_data_dir + os.sep + input_SA_range
    elif input_method == 'ranges':
        ranges = [os.path.basename(range_dir) for range_dir in sorted(get_directories(input_data_dir),
                                                                      key=numsort_dirs)]
    elif input_method == 'compare_SA':
        SA_dirs = []
        temp_SA_dirs = get_directories(input_data_dir)
        for temp_SA_dir in temp_SA_dirs:
            if os.path.basename(temp_SA_dir) in input_SA_dirs:
                SA_dirs.append(os.path.basename(temp_SA_dir))
        SA_dirs = sorted(SA_dirs, key=numsort_dirs)

    # _________________________________________________
    # get all possible data files
    input_fbasenames_all: list[str] = []
    if input_method == 'original' or input_method == 'MCV':
        input_fbasenames_all += get_file_basenames(rsrc_dir=input_data_dir,
                                                   ext=input_file_ext
                                                   )
    elif input_method == 'ranges':
        for rng in ranges:
            input_temp_fnames = []
            input_temp_fnames += get_file_basenames(rsrc_dir=input_data_dir + os.sep + rng,
                                                    ext=input_file_ext
                                                    )
            for i in range(0, len(input_temp_fnames)):
                input_temp_fnames[i] = rng + os.sep + input_temp_fnames[i]
            input_fbasenames_all += input_temp_fnames
    elif input_method == 'compare_SA':
        for SA_dir in SA_dirs:
            input_temp_fnames = []
            input_temp_fnames += get_file_basenames(rsrc_dir=input_data_dir + os.sep + SA_dir + os.sep + input_SA_range,
                                                    ext=input_file_ext
                                                    )
            for i in range(0, len(input_temp_fnames)):
                input_temp_fnames[i] = SA_dir + os.sep + input_SA_range + os.sep + input_temp_fnames[i]
            input_fbasenames_all += input_temp_fnames

    # _________________________________________________
    # get the wanted derivative order
    if input_method != 'original':
        input_fbasenames_all = list(filter(lambda ifn: input_derivative in ifn, input_fbasenames_all))

    # _________________________________________________
    # get the right input files
    input_fbasenames: list[str] = []
    if input_method == 'original' or input_method == 'MCV':
        if input_files_to_plot:
            for input_fbasename_all in input_fbasenames_all:
                if input_fbasename_all in input_files_to_plot:
                    input_fbasenames.append(input_fbasename_all)
        if not input_files_to_plot:
            input_fbasenames = input_fbasenames_all
        input_fbasenames = sorted(input_fbasenames,
                                  key=measurement_control_variable_value
                                  )
    elif input_method == 'ranges':
        pattern_fname = input_files_to_plot[0]
        for input_fbasename_all in input_fbasenames_all:
            matched_fname = re.search(pattern_fname, input_fbasename_all)
            if matched_fname:
                input_fbasenames.append(input_fbasename_all)
    elif input_method == 'compare_SA':
        if input_files_to_plot:
            pattern_fname = input_files_to_plot[0]
            for input_fbasename_all in input_fbasenames_all:
                matched_fname = re.search(pattern_fname, input_fbasename_all)
                if matched_fname:
                    input_fbasenames.append(input_fbasename_all)
        else:
            for input_fbasename_all in input_fbasenames_all:
                input_fbasenames.append(input_fbasename_all)

    # _________________________________________________
    # reverse order of input files
    if input_reverse:
        input_fbasenames = input_fbasenames[::-1]

    # _________________________________________________
    # export input files
    if export_active:
        if input_method != 'MCV' and input_method != 'ranges':
            print('ERROR: export for OriginLab only supported for "MCV" and "ranges" methods!')
            sys.exit(1)
        dot_split_input_fname = input_fbasenames[0].split('.')
        if input_method == 'MCV':
            # example: SA_FFT1.d1.T0p34K.HfSiS.r1.dat
            export_fname: str = '.'.join((dot_split_input_fname[0], input_method, dot_split_input_fname[1],
                                          dot_split_input_fname[3], input_SA_range, input_file_ext)
                                         )
        if input_method == 'ranges':
            # example: r1\SA_FFT1.d1.T0p34K.HfSiS.r1.dat with '\' being the os.sep here
            export_fname = '.'.join((dot_split_input_fname[0].split(os.sep)[-1], input_method, dot_split_input_fname[1],
                                     dot_split_input_fname[2], dot_split_input_fname[3], input_file_ext)
                                    )

        # check whether unique combination of save directory core absolute path and index already exists
        if not os.path.exists(export_dir):
            # create unique save directory
            os.makedirs(export_dir)

        # export data
        print('Start data export')

        file_lengths = []
        for input_fbasename in input_fbasenames:
            input_fpath = input_data_dir + os.sep + input_fbasename
            with open(input_fpath, 'r') as ifp:
                ifp_lines = ifp.readlines()
                file_lengths.append(len(ifp_lines))
        max_file_length = max(file_lengths)
        # print(max_file_length)
        data_files_lines = []
        file_counter = 1
        for input_fbasename in input_fbasenames:
            print(f'Exporting {input_fbasename}')
            input_fpath = input_data_dir + os.sep + input_fbasename
            with open(input_fpath, 'r') as ifp:
                ifp_lines = ifp.readlines()
                ifp_line1_elems = ifp_lines[1].strip('#').strip('\n').split(input_delim)
                if input_method == 'ranges':
                    # swap around MCV and range label
                    if len(ifp_line1_elems) > 2:
                        ifp_lines[1] = '#' + ifp_line1_elems[1]
                        for i in range(1, len(ifp_line1_elems)):
                            ifp_lines[1] = ifp_lines[1] + input_delim + ifp_line1_elems[0]
                        ifp_lines[1] = ifp_lines[1] + '\n'
                    else:
                        ifp_lines[1] = '#' + ifp_line1_elems[1] + input_delim + ifp_line1_elems[0] + '\n'
                while len(ifp_lines) < max_file_length:
                    ifp_lines.append((len(ifp_line1_elems)-1)*input_delim)
                if file_counter == 1:
                    for ifp_line_index, ifp_line in enumerate(ifp_lines):
                        data_files_lines.append(ifp_line.strip('\n').strip('#'))
                elif file_counter > 1:
                    for ifp_line_index, ifp_line in enumerate(ifp_lines):
                        temp_line = data_files_lines[ifp_line_index]
                        data_files_lines[ifp_line_index] = temp_line + input_delim + ifp_line.strip('\n').strip('#')

            file_counter += 1

        with open(export_dir + os.sep + export_fname, 'w') as efp:
            for i in range(0, 2):
                data_files_lines[i] = '#' + data_files_lines[i]
            for data_files_line in data_files_lines:
                efp.write(data_files_line + '\n')

        print('Done exporting')

        if export_only:
            print('Finished only exporting')
            sys.exit(0)

    # _________________________________________________
    # plot original data
    if input_method == 'original' or input_method == 'MCV' or input_method == 'ranges' or input_method == 'compare_SA':
        fig, ax = plt.subplots()

        if not plot_scatter:
            cmap = cmap_colors(np.linspace(0, 1, len(input_fbasenames)))
            ax.set_prop_cycle('color', cmap)

        if plot_offset:
            plot_offset_list = []
            for i in range(0, len(input_fbasenames)):
                plot_offset_list.append(i * plot_offset)
            plot_offset_cycle = itertools.cycle(plot_offset_list)
        else:
            plot_offset_cycle = None

        # list of all lines
        lines = []

        if plot_scatter:
            if plot_scatter_theory:
                if plot_scatter_DFTTony:
                    color_counter = 0
                    if plot_scatter_DFT_ls_colors:
                        assert len(plot_scatter_DFT_ls_fpaths) == len(plot_scatter_DFT_ls_colors)
                    for band in plot_scatter_DFT_ls_fpaths:
                        with open(band, 'r') as fp:
                            x, y = np.loadtxt(fp,
                                              delimiter=input_delim,
                                              skiprows=1,
                                              unpack=True)

                        if plot_cos_scale:
                            y = y * np.cos(x*np.pi/180.0)

                        scat = ax.scatter(x,
                                          y,
                                          s=10.0,
                                          c=plot_scatter_DFT_ls_colors[color_counter],
                                          marker='.',
                                          alpha=1.0,
                                          zorder=2)
                        scat.set_clip_on(True)

                        color_counter += 1

        # loop over all files
        for input_fbasename in input_fbasenames:
            if plot_only_theory:
                continue
            input_fpath = input_data_dir + os.sep + input_fbasename

            input_file_info: dict[str, Any] = create_file_info(rsrc_dir=input_data_dir,
                                                               fbasename=input_fbasename,
                                                               delimiter=input_delim,
                                                               usecols=input_use_columns,
                                                               var_names_line=input_var_names_line_no,
                                                               MCV_name=input_MCV_name,
                                                               MCV_units=input_MCV_units
                                                               )

            x, y = load_data(fpath=input_fpath,
                             delimiter=input_delim,
                             skiprows=input_skiprows,
                             usecols=input_use_columns
                             )

            if not plot_scatter and plot_cos_scale:
                x = x * np.cos(input_file_info['MCV_value']*np.pi/180.0)

            if input_subtract_first_value:
                y = y - y[0]

            if input_normalize_by_mean:
                y = y / np.mean(y)

            if plot_offset_cycle:
                offset = next(plot_offset_cycle)
                y = y + offset

            if not plot_scatter:
                if plot_xlb is not None and plot_xub is not None:
                    bool_idx_arr = (x >= plot_xlb) * (x <= plot_xub)
                    x = x[bool_idx_arr]
                    y = y[bool_idx_arr]
            else:
                if plot_ylb is not None and plot_yub is not None:
                    bool_idx_arr = (x >= plot_ylb) * (x <= plot_yub)
                    x = x[bool_idx_arr]
                    y = y[bool_idx_arr]

            # decide between normal plot or scatter
            if not plot_scatter:
                # plot data and setup label for legend
                if input_method == 'original' or input_method == 'MCV':
                    line, = ax.plot(x, y, label=f"{input_file_info['MCV_value']} {input_file_info['MCV_units']}")
                elif input_method == 'ranges':
                    with open(input_fpath, 'r') as ifp:
                        for i in range(0, 1):
                            next(ifp)
                        line_legend_label = ifp.readline().split(input_delim)[0].strip('#')
                    line, = ax.plot(x, y, label=f"{line_legend_label}")
                elif input_method == 'compare_SA':
                    line, = ax.plot(x, y, label=f"{os.path.basename(os.path.dirname(os.path.dirname(input_fpath)))}")
            else:
                if input_method == 'MCV':
                    # define scatter plot data
                    x_scatter = input_file_info['MCV_value'] * np.ones(shape=len(x))

                elif input_method == 'ranges':
                    with open(input_fpath, 'r') as ifp:
                        for i in range(0, 1):
                            next(ifp)
                        field_range_label = ifp.readline().split(input_delim)[0].strip('#').replace('T', '')\
                            .replace(' ', '')
                    field_range = field_range_label.split('-')
                    field_range_lower = float(field_range[0])
                    field_range_upper = float(field_range[1])
                    avgBinv_inv = 2./((1./field_range_lower) + (1./field_range_upper))
                    x_scatter = avgBinv_inv * np.ones(shape=len(x))

                y_scatter = x
                z_scatter = y
                if plot_cos_scale:
                    y_scatter = y_scatter * np.cos(x_scatter*np.pi/180.0)

                cmap = mpl.cm.get_cmap(f'{plot_cmap_name}')
                scat = ax.scatter(x_scatter,
                                  y_scatter,
                                  s=plot_scatter_size,
                                  c=z_scatter,
                                  cmap=cmap,
                                  marker=plot_scatter_marker,
                                  vmin=plot_scatter_vmin_threshold*max(z_scatter),
                                  vmax=plot_scatter_vmax_scaled*max(z_scatter),
                                  alpha=plot_scatter_alpha,
                                  zorder=1
                                  )
                scat.set_clip_on(True)




            # append to lines
            if not plot_scatter:
                lines.append(line)

            # set x-/y-axis labels
            if input_method == 'original':
                ax.set_xlabel(input_file_info['x_var_name'])
                ax.set_ylabel(input_file_info['y_var_name'])
            else:
                if not plot_scatter:
                    with open(input_fpath, 'r') as ifp:
                        line_xy_labels = ifp.readline().strip('#').split(input_delim)
                    ax.set_xlabel(line_xy_labels[0])
                    ax.set_ylabel(line_xy_labels[1])
                else:
                    ax.set_xlabel('deg')
                    ax.set_ylabel('freq')

        if not plot_scatter:
            # create legend
            if input_reverse:
                handles, labels = ax.get_legend_handles_labels()
                leg = ax.legend(reversed(handles), reversed(labels), ncol=plot_legend_ncol)
                lines = lines[::-1]
            else:
                leg = ax.legend(ncol=plot_legend_ncol)
            for handle in leg.legendHandles:
                handle.set_linewidth(plot_legend_linewidth)
            # making the legend draggable gives continuous KeyError due to clickevent also being linked to the legend
            # handles
            leg.set_draggable(state=plot_legend_draggable)

        # set plot boundaries
        ax.set_aspect('auto')
        ax.set_xbound(lower=plot_xlb, upper=plot_xub)
        ax.set_ybound(lower=plot_ylb, upper=plot_yub)

        # set ticks
        ax.minorticks_on()
        ax.tick_params(axis='both',
                       which='both',
                       direction=plot_ticks_dir,
                       bottom=True,
                       top=True,
                       left=True,
                       right=True)
        xminor_locator = tck.AutoMinorLocator(5)
        yminor_locator = tck.AutoMinorLocator(2)
        ax.xaxis.set_minor_locator(xminor_locator)
        ax.yaxis.set_minor_locator(yminor_locator)

        # set axis notation
        ax.ticklabel_format(style='scientific', useMathText=True)

        if not plot_scatter:
            # for visible/non-visible lines
            lined = {}  # Will map legend lines to original lines.
            for legline, origline in zip(leg.get_lines(), lines):
                legline.set_picker(True)  # Enable picking on the legend line.
                lined[legline] = origline

            def on_pick(event):
                # https://matplotlib.org/stable/gallery/event_handling/legend_picking.html
                # On the pick event, find the original line corresponding to the legend
                # proxy line, and toggle its visibility.
                legline = event.artist
                origline = lined[legline]
                visible = not origline.get_visible()
                origline.set_visible(visible)
                # Change the alpha on the line in the legend so we can see what lines
                # have been toggled.
                legline.set_alpha(1.0 if visible else 0.2)
                fig.canvas.draw()

            fig.canvas.mpl_connect('pick_event', on_pick)
        # show the figure
        plt.show()

    return 0


if __name__ == '__main__':

    # get absolute path to json file (stores plotting settings) from parsed argument
    json_fp = parsing().json_filepath

    # load content from json file into a dictionary
    with open(json_fp) as json_content:
        settings_plotting = json.load(json_content)

    # call main()
    main(settings=settings_plotting)
