# GENERAL IMPORTS
import argparse as argp

import glob  # type: ignore

import io
import itertools as ittls

import json

import math as ma

import numpy as np
import numpy_indexed as npidx  # type: ignore

import openpyxl as opxl  # type: ignore
import openpyxl.styles as opxlsty  # type: ignore
import openpyxl.utils as opxlut  # type: ignore


import os

import re

import scipy.interpolate as scintpl  # type: ignore
import scipy.signal as scsgnl  # type: ignore

import sys

import datetime

from typing import Any, Match, Optional, TextIO, Union

# FFT imports
import scipy.fft as scfft  # type: ignore

# MESA imports
from memspectrum import MESA  # type: ignore
# ____________________________________________________________________________________________________________________ #

# ____________________________________________________________________________________________________________________ #
# GENERAL NOTES
# look at this paper [FermiSurfaceReconstructionFeSeUnderHighPressure] PHYSICAL REVIEW B 93, 094505 (2016)
# Gitlab repository #
# ____________________________________________________________________________________________________________________ #

# ____________________________________________________________________________________________________________________ #
# GENERAL GLOBAL PARAMETERS
# !!!DO NOT CHANGE!!!
# tuple indicating all derivative orders
derivative_orders: tuple[str, str, str] = ('d0', 'd1', 'd2')
# ____________________________________________________________________________________________________________________ #


# ____________________________________________________________________________________________________________________ #
# ____________________________________________________________________________________________________________________ #

# ____________________________________________________________________________________________________________________ #
# GENERAL CLASSES
# Maybe implement later
class Tee(object):
    """
    tee for python
    https://pypi.org/project/pyteetime/
    https://www.rolf-sander.net/software/pyteetime/
    https://linux.101hacks.com/unix/tee-command-examples/
    """
    def __init__(self, user_ioe, sys_active, sys_ioe=None):
        """
        initialize tee-object

        :param user_ioe: StringIO user defined input/output/error
        :param sys_active: boolean that activates usage of _sys_ioe
        :param sys_ioe: TextIO standard system input/output/error
        """

        self.user_ioe = user_ioe
        self.sys_active = sys_active
        self.sys_ioe = sys_ioe

    def write(self, text):
        self.user_ioe.write(text)
        if self.sys_active:
            self.sys_ioe.write(text)

    def flush(self):
        self.user_ioe.flush()
        self.sys_ioe.flush()
# ____________________________________________________________________________________________________________________ #

# ____________________________________________________________________________________________________________________ #
# GENERAL FUNCTIONS


def check_boundaries_x(smallest: float = 0., largest: float = 0., MIN: float = 0., MAX: float = 0.) -> \
        tuple[float, float]:
    """
    function for checking/resetting user-set field range boundaries. They have to fall into the interval defined by
    smallest and largest x-values present in current data set.

    :param smallest: smallest reachable magnetic field value for all input files
    :param largest: largest reachable magnetic field value for all input files
    :param MIN: user input for lower bound of field range (in B-space)
    :param MAX: user input for upper bound of field range (in B-space)
    :return: reset
    """

    # reset user-set field range boundaries if needed
    if MIN < smallest:
        print(f'\tResetting user input for Bmin ({MIN}) as it was smaller than {smallest}\n')
        MIN = smallest
    if MAX > largest:
        print(f'\tResetting user input for Bmax ({MAX}) as it was larger than {largest}\n')
        MAX = largest

    # return original/reset field range boundaries depending on whether the if statements above were True
    return MIN, MAX


def check_calculate_derivative_orders(calculate: dict[str, bool]) -> None:
    """
    function that checks that user made the right selection of algorithm that calculates the wanted derivative orders.

    :param calculate: dictionary of string-boolean (key-value) pairs indicating which derivative order(s) are
                      calculated during interpolation.
    :return: None
    """
    # check that not all derivative calculating algortihms are selected at the same time as d2 includes also calculation
    # of d1
    if all(c_value for c_value in calculate.values()):
        print('ERROR: you can NOT activate calculations of both derivative orders the same time as d2 already'
              'calculates d1!')
        sys.exit(1)


def check_exclusion_derivative_orders(calculate: dict[str, bool], exclude: dict[str, bool]) -> None:
    """
    function that controls that user entries for exclusion of derivative orders is valid as at least 1 order needs to be
    analyzed.

    :param calculate: dictionary of string-boolean (key-value) pairs indicating which derivative order(s) are
                      calculated during interpolation.
    :param exclude: dictionary of string-boolean (key-value) pairs indicating which derivative order(s) are exclude from
                    further analysis.
    :return: None
    """
    # make sure that it is NOT the case that both higher derivative orders will not be calculated and 0th derivative
    # WILL be excluded from further analysis
    if all(not c_value for c_value in calculate.values()) and exclude['d0']:
        print('ERROR: d0 must not be excluded from further analysis if neither d1 nor d2 are calculated!')
        sys.exit(1)

    # check that NOT ALL derivative orders are excluded from further analysis
    if all(e_value for e_value in exclude.values()):
        print('ERROR: at least one derivative order is needed for further analysis!')
        sys.exit(1)

    # check that not both, the 0th and 1st derivative are excluded from further analysis when only derivative orders up
    # to the 1st derivative are calculated
    if calculate['d1']:
        if exclude['d0'] and exclude['d1']:
            print('ERROR: at least one derivative order is needed for further analysis!')
            sys.exit(1)


def check_files_for_data_loading(rsrc_dir: str, fbasenames: list[str], delimiter: str = '\t',
                                 skiprows: int = 0, usecols: tuple[int, int] = (0, 1), data_type=float) -> None:
    """
    function that takes a list of files and checks whether the lines of each file and the data in each data row can be
    loaded in without error.
    Rewrites input file if a faulty behaving line or data entry value in a data row (read line) is found.
    Heavy inspiration is taken from 'numpy.loadtxt'.

    :param rsrc_dir: string indicating directory where all input files are
    :param fbasenames: list of strings containing base name of each input file
    :param delimiter: string indicating delimiter (DEFAULT is '\t')
    :param skiprows: integer indicating how much lines need to be skipped before getting to data rows (DEFAULT is '0')
    :param usecols: tuple of 2 integers defining which 2 columns are checked. (DEFAULT is 1st (0) and 2nd (1) column)
    :param data_type: dtype object indicating what data type is wanted (DEFAULT is 'float')
    :return: returns None
    """
    # loop over files
    for fbasename in fbasenames:

        # define file path by using resource directory and base name of file
        fpath: str = rsrc_dir + os.sep + fbasename

        # define list that stores all header lines
        header: list[str] = []
        # define list to store booleans
        # True: line is proper and works normal for data loading
        # False: line is faulty and will be excluded when rewriting input file
        proper: list[bool] = []

        # open file
        with open(fpath, 'r') as fp:
            # inform user that screening is starting
            print(f'\t\tScreening file {fbasename}')

            # skip indicated number of rows
            for _ in range(0, skiprows):
                header.append(fp.readline())

            # read remaining lines of file and store them
            lines: list[str] = fp.readlines()

        # check each line and the data therein for non proper behaviour
        # if non proper behaviour is detected append respective boolean value to proper
        for line in lines:

            # strip each line of newline character and split according to delimiter
            # generates a list containing the data in this line
            data_value_entries: list[str] = line.strip().split(delimiter)

            # check whether splitting delimiter worked
            if not len(data_value_entries) > 1:
                proper.append(False)
                # continue with next line
                continue

            # check whether data value entries can be converted to wanted data type
            # define boolean value that helps in registering non proper data value entry only once in a data row
            assigned: bool = False
            # select only the wanted columns by making use of usecols
            data_value_entries = [data_value_entries[usecol] for usecol in usecols]
            for data_value_entry in data_value_entries:
                if not is_digit(data_value_entry, data_type) and not assigned:
                    assigned = True
                    break
            if assigned:
                proper.append(False)
                # continue with next line
                continue

            # append True for proper behaviour if none of the if cases above resulted in appending False
            proper.append(True)

        # check that proper has the same length as lines
        assert len(proper) == len(lines)

        # rewrite file if there is any (1 or more) False entry in proper
        if not all(proper):
            # inform user that there were problems in input file
            print(f'\t\t\tScreening revealed problems!')

            # filter out all lines that are not properly behaving for normal data loading
            lines = list(ittls.compress(lines, proper))

            # inform user that rewriting starts
            print(f'\t\t\tRewriting content...\n')
            with open(fpath, 'w') as fp:
                # use the same header as originally
                for hline in header:
                    fp.write(hline)
                # only write proper lines to file
                for line in lines:
                    fp.write(line)
        else:
            # inform user that there no problems were encountered
            print(f'\t\t\tFile is OK\n')


def check_spectral_analysis_bools(fft: bool, mem: bool) -> None:
    """
    function that checks whether any spectral analysis method has been selected at all.
    Also warns user about use of multiple spectral analysis methods at the same time.

    :param fft: boolean indicating whether calculation of Fast Fourier Transform (FFT) is active (True)
    :param mem: boolean indicating whether calculation of Maximum Entropy Method (MEM) is active (True)
    :return: None
    """
    # check whether any spectral analysis method is selected
    if not any([fft, mem]):
        print('ERROR: please choose method for spectral analysis')
        sys.exit(1)
    # warn for selecting all spectral analysis methods at the same time
    if all([fft, mem]):
        print('WARNING: it is not recommended to perform MESA and FFT at the same time!')


def clean_up_spectral_analysis_log(log_file_path: str) -> None:
    """
    function that cleans up the spectral analysis log for MEM spectral analysis method and only keeping information
    on the last iteration.

    :param log_file_path: string indicating absolute path to log file
    :return: None
    """

    # open log file and store lines
    with open(log_file_path, 'r') as lfp:
        lines: list[str] = lfp.readlines()

    # remove certain special characters (carriage escape \r)
    for i in range(0, len(lines)):
        lines[i] = lines[i].strip('\r')

    # rewrite log file and skip unnecessary information from MEM output over recursive iteration status
    with open(log_file_path, 'w') as lfp:

        for i in range(0, len(lines) - 1):

            # save all lines that have nothing to do with MEM output
            if lines[i] and ('Iteration' not in lines[i]):

                lfp.write(lines[i])

            # save only LAST line of MEM output indicating final value of recursive order m used for MEM
            if ('Iteration' in lines[i]) and ('Iteration' not in lines[i+1]):

                lfp.write(lines[i])


def combine_and_write_to_log(*args, dir_path: str = '', mem_active: bool = False) -> int:
    """
    function takes 1 to 2 different log streams and combines them if there are 2 present.

    :param args: 1 or 2 Teed log streams
    :param dir_path: string indicating directory where generated main log file is saved
    :param mem_active: boolean indicating whether MEM is used for spectral analysis
    :param extra_filename: string that can be added to name of main log file
    :return: integer 0
    """

    # assert proper variable declaration
    assert dir_path != ''
    assert 0 < len(args) <= 2

    # define name of main log file
    main_log: str = f'{dir_path}{os.sep}{os.path.basename(dir_path)}_analysis.log'

    # set type of write status
    # write_mode: str = ''
    if not os.path.exists(main_log):
        write_mode = 'w'
    else:
        write_mode = 'a'

    # open log file
    with open(main_log, mode=write_mode) as mlg:
        # write separator and date time
        mlg.write(40 * '_' + '\n')
        mlg.write(str(datetime.datetime.now()) + '\n')
        # loop over individual log streams
        for log in args:
            # get log stream value
            log_contents: str = log.getvalue()
            # write log stream value to main log file
            mlg.write(log_contents)
        # finishing lines
        mlg.write('\n\n')

    # check status of MEM analysis
    if mem_active:
        # due to tee-ing extra output is teed from stderr to stdout during call of spectrum method in memspectrum.py
        # this needs clean-up of final generated log file
        # clean up final generated log file
        clean_up_spectral_analysis_log(log_file_path=main_log)

    return 0


def correct_interpolator_xvalues(x: np.ndarray, N: int) -> tuple[np.ndarray, float]:
    """
    function for newly calculating sampling interval and x values where interpolator function will be evaluated at.

    :param x: np.ndarray of x-data that will be interpolated
    :param N: integer indicating total number of points used for interpolation
    :return: tuple containing np.ndarray and float indicating x values for evaluation of interpolator function and
             sampling interval, respectively.
    """

    # create new sampling interval
    dx = (x[-1] - x[0]) / N
    # return new x values where interpolator function will be evaluated at, as well as the new sampling interval
    return np.arange(x[0], x[-1], dx), dx


def create_file_info(rsrc_dir: str, fbasename: str, delimiter: str = '\t', usecols: tuple[int, int] = (0, 1),
                     var_names_line: Optional[int] = None, MCV_name: str = 'T', MCV_units: str = 'K') -> dict[str, Any]:
    """
    function that creates a dictionary containing all necessary information of input file that is handled at the moment.
    NOTE: the standard file base name should look like:
            <sample_name>.<original_file_no>.<MeasurementControlVariable>.<extension>
    :param rsrc_dir: string indicating absolute path of directory to where all input files are
    :param fbasename: string indicating base name of input file that is handled at the moment
    :param delimiter: string indicating the delimiter used in all input files
                      DEFAULT is '\t', i.e. TAB
    :param usecols: tuple containing 2 integers indicating the columns used for spectral analysis for all input files
    :param var_names_line: integer indicating the line number in an input file where the names of the measured
                           quantities are stored
    :param MCV_name: string indicating name of Measurement Control Variable, i.e. temperature/angle/field/...
    :param MCV_units: string indicating units of Measurement Control Variable, i.e. K/deg/T/...
    :return: dictionary with string key and Any-type value pairs containing all necessary information on input file
    """

    # generate an empty dictionary
    finfo: dict[str, Any] = {}

    # store resource directory
    finfo.update({'resource_directory': rsrc_dir})

    # store file base name
    finfo.update({'file_basename': fbasename})

    # store sample name
    finfo.update({'sample_name': fbasename.split('.')[0]})

    # store original file number [Optional]
    try:
        orig_file_no_match: Optional[Match[str]] = re.search(r'\.f\d+\.', fbasename)
        assert orig_file_no_match is not None
        finfo.update({'original_file_no': orig_file_no_match.group().replace('.', '')})
    except AssertionError:
        finfo.update({'original_file_no': None})

    # store all info on MCV
    finfo.update({'MCV_name': MCV_name})
    finfo.update({'MCV_value': measurement_control_variable_value(fbasename)})
    finfo.update({'MCV_units': MCV_units})
    finfo.update({'MCV_str': MCV_name + str(measurement_control_variable_value(fbasename)).replace('.', 'p') +
                  MCV_units}
                 )

    # !!!ONLY extract names of variables used for spectral analysis IF there exists a line in each input file where the
    # different variable names are defined, i.e. if var_names_line is NOT None!!!
    if var_names_line:
        # define absolute path to input file
        fpath = rsrc_dir + os.sep + fbasename
        # open file to extract names of variable used for spectral analysis
        with open(fpath, 'r') as fp:
            # skip lines until line with variable names is reached
            for _ in range(0, var_names_line - 1):
                next(fp)
            # only read line containing names of all measured variables
            line: str = fp.readline()

        # remove trailing newline character and split according to delimiter
        all_var_names: list[str] = line.strip().split(delimiter)

        # store the used variable names of x- and y-variable by using usecols
        finfo.update({'x_var_name': all_var_names[usecols[0]]})
        finfo.update({'y_var_name': all_var_names[usecols[1]]})
    # else just assign None as variable names for used x- and y-variable
    else:
        finfo.update({'x_var_name': None})
        finfo.update({'y_var_name': None})

    return finfo


def documentation_edit_layout(documentation) -> None:
    """
    function that arranges the layout of the .xlsx documentation
    :param documentation: openpyxl Workbook storing all needed parameters
    :return: None
    """

    # 1: adjust width of each column to its cell width with the largest length
    # 2: adjust font of column headers in row 1 for each sheet
    # https://stackoverflow.com/questions/69335018/openpyxl-find-the-maximum-length-of-a-cell-for-each-column
    for sheet in documentation.worksheets:
        for col in sheet.iter_cols():
            maxLen: int = 0
            col_name: str = ''
            for cell in col:
                if col_name == '':
                    col_name = opxlut.cell.coordinate_from_string(cell.coordinate)[0]
                if cell.value:
                    maxLen = len(str(cell.value)) if len(str(cell.value)) > maxLen else maxLen
            sheet.column_dimensions[col_name].width = maxLen + 5
        for row in sheet.iter_rows(max_row=1):
            for cell in row:
                sheet[f'{cell.coordinate}'].font = opxlsty.Font(bold=True)
                sheet[f'{cell.coordinate}'].border = opxlsty.Border(bottom=opxlsty.Side(border_style='double',
                                                                                        color='00000000'
                                                                                        )
                                                                    )

        # finally, lock the sheet against editing
        sheet.protection.sheet = True


def documentation_update_settings_general(documentation, settings_input: dict[str, Any],
                                          settings_output: dict[str, Any], settings_fieldrange: dict[str, Any],
                                          settings_interpolation: dict[str, Any], settings_zeropadding: dict[str, Any],
                                          settings_fft: dict[str, Any], settings_mem: dict[str, Any]) -> None:
    """
    function for saving the general settings into the documentation spreadsheet.

    :param documentation: openpyxl workbook used for documenting all settings
    :param settings_input: dictionary containing input settings
    :param settings_output: dictionary containing output settings
    :param settings_fieldrange: dictionary containing field range settings
    :param settings_interpolation: dictionary containing interpolation settings
    :param settings_zeropadding: dictionary containing zero padding  settings
    :param settings_fft: dictionary containing FFT settings
    :param settings_mem: dictionary containing MEM settings
    :return: None
    """

    # ---------------------------------------------------------------------------------------------------------------- #
    # INPUT SETTINGS
    # create worksheet to save input settings
    sheet_settings_input = documentation.create_sheet('Input')

    # access library containing dictionaries that are input file info's
    _Library_input_file_info: dict[str, dict[str, Any]] = settings_input['Library_input_file_info']

    # document sample name
    _sample_name: str = _Library_input_file_info[list(_Library_input_file_info.keys())[0]]['sample_name']
    sheet_settings_input['A1'] = 'Sample'
    sheet_settings_input['A2'] = _sample_name

    # document input file resource directory
    _input_fresource_dir: str = _Library_input_file_info[list(_Library_input_file_info.keys())[0]]['resource_directory']
    sheet_settings_input['B1'] = 'Input File Directory'
    sheet_settings_input['B2'] = _input_fresource_dir

    # document all available input files (base names)
    _input_fbasenames: list[str] = settings_input['input_fbasenames']
    sheet_settings_input['C1'] = 'Available Input Files'
    sheet_settings_input_C_col_index: int = 2
    for available_fbasename in _input_fbasenames:
        sheet_settings_input[f'C{sheet_settings_input_C_col_index}'] = available_fbasename
        sheet_settings_input_C_col_index += 1

    # document file numbers (file_no.) if available
    sheet_settings_input['D1'] = 'File no.'
    sheet_settings_input_D_col_index: int = 2
    for key in _Library_input_file_info.keys():
        sheet_settings_input[f'D{sheet_settings_input_D_col_index}'] = _Library_input_file_info[key]['original_file_no']
        sheet_settings_input_D_col_index += 1

    # document the Measurement Control Variable values
    _MCV_description: str = _Library_input_file_info[list(_Library_input_file_info.keys())[0]]['MCV_name'] + \
                            f' ({_Library_input_file_info[list(_Library_input_file_info.keys())[0]]["MCV_units"]})'
    sheet_settings_input['E1'] = _MCV_description
    sheet_settings_input_E_col_index: int = 2
    for key in _Library_input_file_info.keys():
        sheet_settings_input[f'E{sheet_settings_input_E_col_index}'] = _Library_input_file_info[key]['MCV_value']
        sheet_settings_input_E_col_index += 1

    # document actually used input files (base names)
    # NOTE: compare with _input_fbasenames used in for Col['C]
    # https://stackoverflow.com/questions/12902621/getting-the-row-and-column-numbers-from-coordinate-value-in-openpyxl
    _input_files_to_analyze: list[str] = settings_input['input_files_to_analyze']
    sheet_settings_input['F1'] = 'Analyzed Input Files'
    for cell in sheet_settings_input['C']:
        # get the row coordinate of Col['C']
        row_coordinate: int = opxlut.cell.coordinate_from_string(cell.coordinate)[1]
        # skip the 1st row
        if row_coordinate == 1:
            continue
        # check that _input_files_to_analyze is not empty
        if _input_files_to_analyze and (cell.value in _input_files_to_analyze):
            sheet_settings_input[f'F{row_coordinate}'] = cell.value
        # elif: assign all _input_fbasenames if _input_files_to_analyze is empty
        elif not _input_files_to_analyze:
            sheet_settings_input[f'F{row_coordinate}'] = cell.value

    # document whether files are checked before loading
    sheet_settings_input['G1'] = 'Checking Input Files'
    sheet_settings_input['G2'] = settings_input['CFlow_check_input_files']

    # document line number in which names of measured variables are stored
    sheet_settings_input['H1'] = 'Line no. (variable names)'
    sheet_settings_input['H2'] = settings_input['input_var_names_line_no']

    # document variable names
    _input_use_columns = settings_input['input_use_columns']
    # x-variable (with integer column index)
    # NOTE: unit-offsetting _input_use_columns index
    sheet_settings_input['I1'] = f'x-variable (Column {_input_use_columns[0]})'
    sheet_settings_input['I2'] = _Library_input_file_info[list(_Library_input_file_info.keys())[0]]['x_var_name']
    # y-variable (with integer column index)
    # NOTE: unit-offsetting _input_use_columns index
    sheet_settings_input['J1'] = f'y-variable (Column {_input_use_columns[1]})'
    sheet_settings_input['J2'] = _Library_input_file_info[list(_Library_input_file_info.keys())[0]]['y_var_name']

    # document input file extension
    sheet_settings_input['K1'] = 'Input File Extension'
    sheet_settings_input['K2'] = settings_input['input_file_ext']

    # document how many rows are skipped when loading in data
    sheet_settings_input['L1'] = 'Rows to Skip'
    sheet_settings_input['L2'] = settings_input['input_skiprows']

    # document delimiter used
    sheet_settings_input['M1'] = 'Delimiter'
    sheet_settings_input['M2'] = repr(settings_input['input_delim'])

    # document data type
    sheet_settings_input['N1'] = 'Data Type'
    sheet_settings_input['N2'] = settings_input['input_data_type'].__name__

    # ---------------------------------------------------------------------------------------------------------------- #
    # OUTPUT SETTINGS
    # create worksheet to save output settings
    sheet_settings_output = documentation.create_sheet('Output')

    # document output file extension
    sheet_settings_output['A1'] = 'Output File Extension'
    sheet_settings_output['A2'] = settings_output['output_file_ext']

    # document frequency cutoff used during saving spectral analysis files
    sheet_settings_output['B1'] = 'Frequency Cutoff'
    sheet_settings_output['B2'] = settings_output['output_freq_cutoff']

    # document status of spectrum normalisation by maximum value
    sheet_settings_output['C1'] = 'Normalize by Max'
    sheet_settings_output['C2'] = settings_output['output_norm_by_max']

    # document status of spectrum normalisation by mean value
    sheet_settings_output['D1'] = 'Normalize by Mean'
    sheet_settings_output['D2'] = settings_output['output_norm_by_mean']

    # document status of spectrum normalisation
    sheet_settings_output['E1'] = 'Multiple y Columns'
    sheet_settings_output['E2'] = settings_output['output_multiple_y_cols']

    # document status of comment float formatter
    sheet_settings_output['F1'] = 'Comment Float Format'
    sheet_settings_output['F2'] = settings_output['output_comment_floatformat']

    # ---------------------------------------------------------------------------------------------------------------- #
    # GENERAL INVERSE FIELD RANGE SETTINGS
    # create worksheet to save general inverse field range settings
    sheet_settings_general_ifr = documentation.create_sheet('Field Range - General')

    # document method for determining inverse field ranges
    sheet_settings_general_ifr['A1'] = 'Range Method'
    sheet_settings_general_ifr['A2'] = settings_fieldrange['FieldRng_method']

    # document index of method for determining inverse field ranges (can be used as double check)
    sheet_settings_general_ifr['B1'] = 'Range Method Index'
    sheet_settings_general_ifr['B2'] = settings_fieldrange['FieldRng_methods_idx']

    # document by all input files reachable smallest & largest x-values
    # smallest x-value
    sheet_settings_general_ifr['C1'] = 'Smallest Available Field'
    sheet_settings_general_ifr['C2'] = settings_fieldrange['B_smallest']
    # largest x-value
    sheet_settings_general_ifr['D1'] = 'Largest Available Field'
    sheet_settings_general_ifr['D2'] = settings_fieldrange['B_largest']

    # document field range lower & upper fields (in Tesla)
    # lower field
    sheet_settings_general_ifr['E1'] = 'B_min [Lower Field] (T)'
    sheet_settings_general_ifr['E2'] = settings_fieldrange['FieldRng_Bmin']
    # upper field
    sheet_settings_general_ifr['F1'] = 'B_max [Upper Field] (T)'
    sheet_settings_general_ifr['F2'] = settings_fieldrange['FieldRng_Bmax']

    # document step size
    # NOTE: depending on method this is either in Tesla or 1/Tesla
    sheet_settings_general_ifr['G1'] = "Step Size (only needed for methods starting with 'shift')"
    sheet_settings_general_ifr['G2'] = settings_fieldrange['FieldRng_Bstep']

    # document halting value for shift_MIN / shift_MAX methods
    sheet_settings_general_ifr['H1'] = "Halt Value (only needed for methods starting with 'shift_M')"
    sheet_settings_general_ifr['H2'] = settings_fieldrange['FieldRng_shiftMINMAX_halt']

    # document in which direction both, lower & upper fields are shifted
    sheet_settings_general_ifr['I1'] = "Shift towards larger values on x-axis (only needed for methods starting with " \
                                       "'shift_both')"
    sheet_settings_general_ifr['I2'] = settings_fieldrange['FieldRng_shift_to_larger']

    # document cutoff field
    # NOTE: depending on method this is either in Tesla or 1/Tesla
    sheet_settings_general_ifr['J1'] = "Range Cutoff (only needed for methods starting with 'shift_both')"
    sheet_settings_general_ifr['J2'] = settings_fieldrange['FieldRng_cutoff']

    # ---------------------------------------------------------------------------------------------------------------- #
    # GENERAL INTERPOLATION SETTINGS
    # create worksheet to save general interpolation settings
    sheet_settings_general_intpl = documentation.create_sheet('Interpolation - General')

    # document interpolation method
    sheet_settings_general_intpl['A1'] = 'Interpolation Method'
    sheet_settings_general_intpl['A2'] = settings_interpolation['interpol_method']

    # document index of interpolation method
    sheet_settings_general_intpl['B1'] = 'Interpolation Method Index'
    sheet_settings_general_intpl['B2'] = settings_interpolation['interpol_methods_idx']

    # document manually entered #points user may want for interpolation
    sheet_settings_general_intpl['C1'] = 'MANUAL #Points for Interpolation'
    sheet_settings_general_intpl['C2'] = settings_interpolation['interpol_n_manual']

    # document whether to use #points with base 2 for interpolation
    sheet_settings_general_intpl['D1'] = 'Base 2 for #Points for Interpolation'
    sheet_settings_general_intpl['D2'] = settings_interpolation['interpol_base2_bool']

    # document power (with respect to base 2) defining #points for interpolation
    sheet_settings_general_intpl['E1'] = 'Power (for #Points = 2^Power)'
    sheet_settings_general_intpl['E2'] = settings_interpolation['interpol_power_base2']

    # document whether derivative orders are calculated
    # derivative order name
    sheet_settings_general_intpl['F1'] = 'Deriv. Order (Name)'
    sheet_settings_general_intpl['F2'] = 'd0'
    sheet_settings_general_intpl_F_col_index: int = 3
    for key in settings_interpolation['CFlow_calculate_deriv_orders'].keys():
        sheet_settings_general_intpl[f'F{sheet_settings_general_intpl_F_col_index}'] = key
        sheet_settings_general_intpl_F_col_index += 1
    # calculation state of derivative order
    sheet_settings_general_intpl['G1'] = 'Calculate Deriv. Order (State)'
    sheet_settings_general_intpl['G2'] = True
    sheet_settings_general_intpl_G_col_index: int = 3
    for value in settings_interpolation['CFlow_calculate_deriv_orders'].values():
        sheet_settings_general_intpl[f'G{sheet_settings_general_intpl_G_col_index}'] = value
        sheet_settings_general_intpl_G_col_index += 1

    # document whether derivative orders are excluded
    # exclusion state of derivative order
    sheet_settings_general_intpl['H1'] = 'Exclude Deriv. Order (State)'
    sheet_settings_general_intpl['H2'] = True
    sheet_settings_general_intpl_H_col_index: int = 2
    for value in settings_interpolation['CFlow_exclude_deriv_orders'].values():
        sheet_settings_general_intpl[f'H{sheet_settings_general_intpl_H_col_index}'] = value
        sheet_settings_general_intpl_H_col_index += 1

    # ---------------------------------------------------------------------------------------------------------------- #
    # GENERAL ZERO PADDING SETTINGS
    # create worksheet to save general zero padding settings
    sheet_settings_general_zpad = documentation.create_sheet('Zero Padding - General')

    # document whether user wants to zero pad
    sheet_settings_general_zpad['A1'] = 'Zero Padding State'
    sheet_settings_general_zpad['A2'] = settings_zeropadding['zeropad_bool']

    # document whether total #points after zero padding is a power of 2
    sheet_settings_general_zpad['B1'] = 'Base 2 for total #Points'
    sheet_settings_general_zpad['B2'] = settings_zeropadding['zeropad_base2_bool']

    # document power (with respect to base 2) defining total #points after zero padding
    sheet_settings_general_zpad['C1'] = 'Power (total #Points = 2^Power)'
    sheet_settings_general_zpad['C2'] = settings_zeropadding['zeropad_power_base2']

    # document how many zero's are padded before first data point
    # NOTE: if zeropad_bool is False then this number is pointless
    sheet_settings_general_zpad['D1'] = '#zeros before'
    sheet_settings_general_zpad['D2'] = settings_zeropadding['zeropad_n_before']

    # ---------------------------------------------------------------------------------------------------------------- #
    # GENERAL FFT SETTINGS
    # create worksheet to save general FFT settings
    sheet_settings_general_fft = documentation.create_sheet('FFT - General')

    # document whether user wants to use FFT spectral anaylsis
    sheet_settings_general_fft['A1'] = 'FFT (State)'
    sheet_settings_general_fft['A2'] = settings_fft['fft_bool']

    # document window function
    sheet_settings_general_fft['B1'] = 'Window Function Name'
    sheet_settings_general_fft['B2'] = settings_fft['window_settings'][0]

    # document index of window function
    sheet_settings_general_fft['C1'] = 'Window Function Index'
    sheet_settings_general_fft['C2'] = settings_fft['window_idx']

    # document window function parameters (if present)
    sheet_settings_general_fft['D1'] = 'Window Function Parameters'
    sheet_settings_general_fft_D_col_index: int = 2
    if len(settings_fft['window_settings']) > 1:
        for i in range(1, len(settings_fft['window_settings'])):
            if isinstance(settings_fft['window_settings'][i], list):
                for list_elem in settings_fft['window_settings'][i]:
                    sheet_settings_general_fft[f'D{sheet_settings_general_fft_D_col_index}'] = list_elem
                    sheet_settings_general_fft_D_col_index += 1
            else:
                sheet_settings_general_fft[f'D{sheet_settings_general_fft_D_col_index}'] = settings_fft['window_'
                                                                                                        'settings'][i]
                sheet_settings_general_fft_D_col_index += 1

    # document whether window function is periodic
    sheet_settings_general_fft['E1'] = 'Window Periodic'
    sheet_settings_general_fft['E2'] = settings_fft['window_periodic']

    # document whether only onesided FFT is calculated
    sheet_settings_general_fft['F1'] = 'Onesided FFT'
    sheet_settings_general_fft['F2'] = settings_fft['fft_onesided']

    # ---------------------------------------------------------------------------------------------------------------- #
    # GENERAL MESA SETTINGS
    # create worksheet to save general MESA settings
    sheet_settings_general_mem = documentation.create_sheet('MESA - General')

    # document whether user wants to use 'MEM spectral anaylsis' (= MESA)
    sheet_settings_general_mem['A1'] = 'MESA (state)'
    sheet_settings_general_mem['A2'] = settings_mem['mem_bool']

    # document loss function
    sheet_settings_general_mem['B1'] = 'Loss Function'
    sheet_settings_general_mem['B2'] = settings_mem['mem_opt_method']

    # document loss function index
    sheet_settings_general_mem['C1'] = 'Loss Function Index'
    sheet_settings_general_mem['C2'] = settings_mem['mem_opt_methods_idx']

    # document Burg's method
    sheet_settings_general_mem['D1'] = "Burg's Method"
    sheet_settings_general_mem['D2'] = settings_mem['mem_Burg_method']

    # document Burg's method index
    sheet_settings_general_mem['E1'] = "Burg's Method Index"
    sheet_settings_general_mem['E2'] = settings_mem['mem_Burg_methods_idx']

    # document Tikhonov regularization parameter
    sheet_settings_general_mem['F1'] = 'Tikhonov Regularization'
    sheet_settings_general_mem['F2'] = settings_mem['mem_Tikh_reg']

    # document wheter MEM is stopped early if no optimal parameters can be found
    sheet_settings_general_mem['G1'] = 'Early Stop (State)'
    sheet_settings_general_mem['G2'] = settings_mem['mem_early_stop']

    # document whether MEM info is printed
    sheet_settings_general_mem['H1'] = 'Verbose (State)'
    sheet_settings_general_mem['H2'] = settings_mem['mem_verbose']

    # document whether only onesided MEM is calculated
    sheet_settings_general_mem['I1'] = 'Onesided MESA'
    sheet_settings_general_mem['I2'] = settings_mem['mem_onesided']

    # document initial recursive order m
    sheet_settings_general_mem['J1'] = 'Initial Recursive Order (m)'
    sheet_settings_general_mem['J2'] = settings_mem['mem_recur_order_init']


def documentation_update_settings_range_specific(documentation, settings_rangespecific: dict[str, Any]) -> None:
    """
    function that saves all range specific info into documentation workbook

    :param documentation: openpyxl workbook used for documenting all settings
    :param settings_rangespecific: large dictionary of the form:
                                   {r1: {r1_interval: (<interval>),
                                         r1_Library_frsi: {<fname1>: {<frsi_dict>}
                                                           }
                                         },
                                    r2: ...,
                                    .
                                    .
                                    .
                                   }
    :return: None
    """
    # loop over all documented field ranges ranges <ri> with i being an integer starting at 1
    for fieldrange_key in settings_rangespecific.keys():

        # create worksheet to save specific settings for a single range
        sheet_settings_ri = documentation.create_sheet(fieldrange_key)

        # access field range tuple and range specific file Library
        # NOTE: frsi = file range specific info
        _range_interval: tuple[float, float] = settings_rangespecific[fieldrange_key][f'{fieldrange_key}_interval']
        _Library_frsi: dict[str, Any] = settings_rangespecific[fieldrange_key][f'{fieldrange_key}_Library_file_range_'
                                                                               'specific_info']

        # document field range interval (in Tesla)
        sheet_settings_ri.merge_cells('A1:B1')
        sheet_settings_ri['A1'] = f'{fieldrange_key}_interval (T)'
        sheet_settings_ri['A2'] = 1. / _range_interval[1]
        sheet_settings_ri['B2'] = 1. / _range_interval[0]

        # document field range interval (in 1/Tesla)
        sheet_settings_ri.merge_cells('D1:E1')
        sheet_settings_ri['D1'] = f'{fieldrange_key}_interval (1/T)'
        sheet_settings_ri['D2'] = _range_interval[0]
        sheet_settings_ri['E2'] = _range_interval[1]

        # define starting column for saving for each analyzed file the range specific info
        Col_frsi_start: str = 'G'
        # initialize column index for storing frsi
        Col_frsi_index: int = opxlut.column_index_from_string(Col_frsi_start)

        # loop over library (it is a dictionary) that has the following key-value pair structure:
        #   <analyzed_filename>: dict(range specific settings for each <analyzed_filename>)
        for fname_key, frsi in _Library_frsi.items():

            # get the strings of both columns used for storing frsi per file
            Col1: str = opxlut.get_column_letter(Col_frsi_index)
            Col2: str = opxlut.get_column_letter(Col_frsi_index + 1)

            # merge row 1 of Col1 and Col2 and display file name
            sheet_settings_ri.merge_cells(f'{Col1}1:{Col2}1')
            sheet_settings_ri[f'{Col1}1'] = fname_key

            # set starting row for storing frsi content
            row_start: int = 2
            # loop over frsi dictionary content
            for i, frsi_key in enumerate(_Library_frsi[fname_key].keys()):
                sheet_settings_ri[f'{Col1}{i + row_start}'] = frsi_key
                sheet_settings_ri[f'{Col2}{i + row_start}'] = _Library_frsi[fname_key][frsi_key]

            # advance Col_frsi_index for next file with leaving one empty column in between
            Col_frsi_index += 3


def float_or_evaluate_str_expression(float_or_str_expr: Union[float, str]) -> float:
    try:
        conv_val: float = float(float_or_str_expr)
    except ValueError:
        conv_val = eval(float_or_str_expr)
    return conv_val


def get_file_basenames(rsrc_dir: str, specific: Optional[list[str]] = None, exclude: bool = False,
                       ext: str = 'dat') -> list[str]:
    """
    function that returns list of base names of matching files present in a resource directory.
    Starting directory is resource directory.
    Option to include extra string for filtering file base names.

    :param rsrc_dir: resource directory from which to get file paths
    :param specific: OPTIONAL string for only getting specific file paths
    :param ext: extension of files. DEFAULT is '.dat'
    :return: list of base names of each file in rsrc_dir

    """
    # check whether resource directory that has been given is valid
    if not os.path.exists(rsrc_dir) or not os.path.isdir(rsrc_dir):
        raise FileNotFoundError('Enter valid resource directory for input files!')

    # obtain unsorted list containing all base names of the file paths
    filepaths: list[str] = glob.glob(f'*.{ext}', root_dir=rsrc_dir)

    try:
        # check that not an empty list was returned from glob.glob(...)
        filepaths[0]
    except IndexError as e:
        print(e.__doc__)
        print("ERROR: no file paths available")

    if specific:
        assert isinstance(specific, list)
        # filter for file path base names with specific string
        if not exclude:
            filepaths = [fpath for fpath in specific if fpath in filepaths]
        else:
            filepaths = [fpath for fpath in filepaths if fpath not in specific]
        if not filepaths:
            # check that filtering for specific did not return an empty list
            print(f'ERROR: {specific} can not be found in file base names in resource directory {rsrc_dir}')
            sys.exit(1)

    return filepaths


def get_smallest_and_largest_x(rsrc_dir: str, fbasenames: list[str], delimiter: str = '\t', skiprows: int = 0,
                               usecols: tuple[int, int] = (0, 1), data_type=float) -> tuple:
    """
    function that extracts the reachable smallest and largest x-data value from files that form a dataset.
    Reachable means that the smallest and largest x-data values lie within the range of each file of the dataset as not
    each recorded file during measurement is the same.

    :param rsrc_dir: string indicating directory where all input files are
    :param fbasenames: list of strings containing base name of each input file
    :param delimiter: string indicating delimiter (DEFAULT is '\t')
    :param skiprows: integer indicating how much lines need to be skipped before getting to data rows (DEFAULT is '0')
    :param usecols: tuple of 2 integers defining which 2 columns are checked. (DEFAULT is 1st (0) and 2nd (1) column)
    :param data_type: dtype object indicating what data type is wanted (DEFAULT is 'float')
    :return: tuple containing reachable lowest and highest x-data values
    """

    # define list to store all candidates of smallest x-data value
    x_smallest: list = []
    # define list to store all candidates of largest x-data value
    x_largest: list = []

    # loop over input files
    for fbasename in fbasenames:
        # create file path
        fpath: str = rsrc_dir + os.sep + fbasename

        # open input file from file path
        with open(fpath, 'r') as fp:
            # skip rows containing header
            for _ in range(0, skiprows):
                next(fp)
            # store the rest of the file
            lines: list[str] = fp.readlines()

        # define list that stores all x-data values of a file
        x_values: list = []

        # loop over all available lines to extract x-data values
        for line in lines:
            # strip each line of newline character and split according to delimiter
            # generates a list containing the data in this line
            data_value_entries: list[str] = line.strip().split(delimiter)

            # select only the wanted columns by making use of usecols
            data_value_entries = [data_value_entries[usecol] for usecol in usecols]

            # map to x-data entry value to wanted data type and append to list storing all x-data values of a file
            x_values.append(data_type(data_value_entries[0]))

        # select from the x-data values the smallest x-data value that is NOT 0
        x_smallest.append(min(x_val for x_val in x_values if x_val))
        # select from the x-data values the largest x-data value
        x_largest.append(max(x_values))

    # return the largest value in x_smallest and the smallest value in x_largest to make the resulting smallest and
    # largest x-data values reachable for all input files (IMPORTANT for field range selection)
    return max(x_smallest), min(x_largest)


def get_window_function(w_settings: tuple, N: Optional[int] = None, fftbins: bool = True) -> np.ndarray:
    """
    function for getting the window function as np.ndarray based on the user defined settings.
    :param w_settings: tuple containing all parameters needed for window function
    :param N: integer indicating length of input data
    :param fftbins: boolean indicating whether a periodic or symmetric window function is needed.
                    DEFAULT is True as this gives a periodic window function needed for FFT
    :return: np.ndarray containing window function
    """
    # NOTE: for explanation of input parameters see link:
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window
    # in short: fftbins=True -> w is periodic; fftbins=False -> w is symmetric

    # make sure that a data length has been given
    if not N:
        print('ERROR: window function cannot be initialized if length of data is not given!')
        sys.exit(1)

    # get the evaluated window function based on the user defined settings
    # NOTE: first argument has a type: ignore because input is expected to be str | float, but following the docs tuple
    #       is also allowed allowed and should raise no type mismatch problems...
    w: np.ndarray = scsgnl.get_window(w_settings,  # type: ignore
                                      Nx=N,
                                      fftbins=fftbins
                                      )

    return w


def interpolate_data(x: np.ndarray, y: np.ndarray, x_values: np.ndarray, calc: dict[str, bool],
                     exclude: dict[str, bool], method: str = '') -> dict[str, Optional[np.ndarray]]:
    """
    function that interpolates data and immediately calculates the derivative(s) up to 1st or 2nd order.

    :param x: np.ndarray containing x-data to be interpolated
    :param y: np.ndarray containing y-data to be interpolated
    :param x_values: np.ndarray containing x values for evaluation of interpolator function
    :param calc: dictionary of string-boolean (key-value) pairs indicating which derivative order(s) are
                 calculated during interpolation.
    :param exclude: dictionary of string-boolean (key-value) pairs indicating which derivative order are excluded from
                    being calculated (True) or not (False)
    :param method: string indicating interpolator function
    :return: tuple containing (multiple) np.ndarray(s) for y-data and its derivative orders.
    """

    # nested function for initializing the interpolator function based on method
    def interpolator(_x: np.ndarray, _y: np.ndarray, _method: str) -> scintpl.UnivariateSpline:
        """
        function that initializes interpolator function based on _method.

        :param _x: np.ndarray containing x-data to be interpolated
        :param _y: np.ndarray containing y-data to be interpolated
        :param _method: string indicating interpolator function
        :return: interpolator function
        """
        # check whether user gave valid input
        if _method == '' or _method != 'linear':
            print(f'ERROR: interpolation method not properly initialized or {_method} is not supported, yet!')
            raise ValueError
        # LINEAR INTERPOLATOR
        elif _method == 'linear':
            return scintpl.UnivariateSpline(_x, _y, k=1, s=0)

    # nested function to calculate 1st derivative of interpolator function
    def first_derivative(_interpolator: scintpl.UnivariateSpline) -> scintpl.UnivariateSpline:
        """
        function calculating 1st derivative of given interpolator function

        :param _interpolator: class in scipy.interpolate object indicating interpolator function
        :return: scintpl.UnivariateSpline object of the interpolator function itself of the 1st derivative
        """

        # initialize the interpolator function representing the 1st derivative
        _d1_interpolator: scintpl.UnivariateSpline = _interpolator.derivative()
        return _d1_interpolator

    # nested function to calculate 2nd derivative of interpolator function
    def second_derivative(_d1_interpolator: scintpl.UnivariateSpline) -> scintpl.UnivariateSpline:
        """
        function calculating the 2nd derivative.

        :param _d1_interpolator: class in scipy.interpolate object indicating interpolator function object of 1st
                                 derivative that MUST be differentiable.
        :return: scintpl.UnivariateSpline object of the interpolator function itself of the 2nd derivative
        """

        # initialize the 2nd derivative of interpolator function object
        _d2_interpolator: scintpl.UnivariateSpline = _d1_interpolator.derivative()
        return _d2_interpolator

    # initialize dictionary storing interpolated y-data
    Y_interpol: dict[str, Optional[np.ndarray]] = {'d0': None,
                                                   'd1': None,
                                                   'd2': None
                                                   }

    # initialize the interpolator function object of derivative order 0
    f_interpol: scintpl.UnivariateSpline = interpolator(x, y, method)

    # # calculate 0th derivative (i.e. just evaluate interpolator function)
    y_interpol: np.ndarray = f_interpol(x_values)

    # update dictionary depending on excluded
    if not exclude['d0']:
        Y_interpol['d0'] = y_interpol

    # calculate 1st derivative
    if calc['d1'] or calc['d2']:
        # initialize the interpolator function object of derivative order 1
        d1_f_interpol: scintpl.UnivariateSpline = first_derivative(f_interpol)

        # calculate the interpolator function of order 1
        d1_y_interpol = d1_f_interpol(x_values)

        # update storage dictionary if 1st derivative is not excluded
        if not exclude['d1']:
            Y_interpol['d1'] = d1_y_interpol

        # only enter here if the boolean indicating 2nd derivative order is True
        if calc['d2']:
            # make first derivative of linear interpolator continuous if needed
            # NOTE: only needed for 'linear' using UnivariateSpline with k=1
            if method == 'linear':
                d1_f_interpol = interpolator(x_values, d1_y_interpol, method)

            # initialize the interpolator function object of derivative order 2
            d2_f_interpol: scintpl.UnivariateSpline = second_derivative(d1_f_interpol)

            # calculate the interpolator function of order 2
            d2_y_interpol = d2_f_interpol(x_values)

            # update storage dictionary if 2nd derivative is not excluded
            if not exclude['d2']:
                Y_interpol['d2'] = d2_y_interpol

    return Y_interpol


def interpolator_xvalues(x: np.ndarray, number_of_points: int) -> tuple[np.ndarray, float]:
    """
    function returning x values where interpolator function will be evaluated at, as well as the sampling interval.

    :param x: np.ndarray containing x-data that will be interpolated
    :param number_of_points: integer indicating how many points will fal into the interval spanned by x-data values
    :return: tuple containing np.ndarray of x values where interpolator will be evaluated at, as well as float
             indicating calculated sampling interval.
    """

    # calculate the sampling interval used for interpolation based on the wanted number of points from original x-data
    dx_intpl = (x[-1] - x[0]) / number_of_points

    # calculate the x-data values where the interpolator function will be evaluated at
    # NOTE: x[-1] will not be included in x_intpl
    x_intpl = np.arange(x[0], x[-1], dx_intpl)

    return x_intpl, dx_intpl


def inverse_field_range(MIN: float, MAX: float) -> tuple[float, float]:
    """
    function for creating a tuple containing the boundaries of an inverse magnetic field range.
    :param MIN: smaller magnetic field value (B-space)
    :param MAX: larger magnetic field value (B-space)
    :return: tuple containing 2 floats, i.e. smaller and larger inverse magnetic field values (1/B-space)
    """

    # remember in 1/B space smaller and larger values are defined the other way around
    invMIN: float = 1 / MAX
    invMAX: float = 1 / MIN

    return invMIN, invMAX


def is_digit(value_str: str, _data_type) -> bool:
    """
    function for checking whether a string can be converted into some data type defined by dtype object _data_type

    :param value_str: string that will be checked whether it can be converted into wanted data type
    :param _data_type: dtype object for converting input string
    :return: boolean indicating either success (True) or failure (False)
    """
    # https://stackoverflow.com/questions/736043/checking-if-a-string-can-be-converted-to-float-in-python
    # try the type conversion
    try:
        _data_type(value_str)
        return True
    # catch possible ValueError
    except ValueError:
        return False


def load_data(fpath: str, delimiter: str, skiprows: int, usecols: tuple[int, int] = (0, 1), comments: str ='#')\
        -> np.ndarray:
    """
    function for loading data with 'numpy.loadtxt'

    :param fpath: string indicating path to input file
    :param delimiter: string indicating delimiter used in input file
    :param skiprows: integer indicating number of lines to skip
    :param usecols: tuple of 2 integers indicating which columns to use. DEFAULT are 1st (0) and 2nd (1) columns.
    :param comments: string that is first character on lines that are to be skipped
    :return: unpacked numpy.ndarray containing x- and y-data
    """
    return np.loadtxt(fpath, comments=comments, delimiter=delimiter, skiprows=skiprows, usecols=usecols, unpack=True)


def make_main_save_directories(save_directories_corebasenames: dict[str, bool], rsrc_dir: str) -> list[str]:
    """
    function that create new numbered save directory for each selected type of spectral analysis

    :param save_directories_corebasenames: dictionary containing string and boolean pairs.
                                           The string indicates the generic name of the type of spectral analysis
                                           (FFT, MESA, ...).
                                           The boolean indicates whether the respective type of spectral analysis is
                                           active (True).
    :param rsrc_dir: string indicating directory where all input files are
    :return: list containing string indicating absolute path to save directory, e.g. [.../FFT1, .../MESA1] if spectral
             analysis is run for the first time on data.
    """

    # check whether user provided all input for creating save directories
    if not save_directories_corebasenames or not rsrc_dir:
        print('ERROR: no directory specified to save output!')
        sys.exit(1)

    # create list storing absolute path to save directory and its index for each type of spectral analysis
    # NOTE: per runtime there is only 1 new save directory created PER spectral analysis method
    save_directories: list[str] = []

    # loop over available core base names (FFT, MESA, ...)
    # NOTE: core means generic, i.e. without any other specifications
    for corebasename, active in save_directories_corebasenames.items():
        # check if analysis type is active
        if active:
            # create a core absolute path
            save_directory_coreabspath: str = rsrc_dir + os.sep + corebasename
            # set the index that identifies the save directory and start looping (while)
            save_directory_index: int = 1
            while True:
                # check whether unique combination of save directory core absolute path and index already exists
                if not os.path.exists(save_directory_coreabspath + str(save_directory_index)):
                    # create unique save directory
                    os.makedirs(save_directory_coreabspath + str(save_directory_index))
                    # append information as length-2 tuple to list that stores all information
                    save_directories.append(save_directory_coreabspath + str(save_directory_index))
                    # break out of 'infinite' while loop
                    break
                # raise save directory index if combination of save directory core absolute path and index already
                # existed
                save_directory_index += 1
    return save_directories


def make_sub_save_directory(range_index: int, main_save_directory_abspath: str) -> str:
    """
    function for creating a directory that stores the output of a spectral analysis method for a certain inverse
    magnetic field range.
    NOTE: range directory is ALWAYS a subdirectory of the MAIN save directory of a spectral analysis method.

    :param range_index: integer counting the different ranges. Starts at 0 but needs to be unit offset
                        (i.e. starting at 1).
    :param main_save_directory_abspath: string indicating absolute path of the MAIN save directory of the spectral
                                        analysis method used.
    :return: string indicating the absolute path of the range directory used for saving the output of 1 inverse magnetic
             field range of the selected spectral analysis method.
    """

    # define unique absolute path to a new range directory
    # NOTE1: range directories are ALWAYS subdirectories of a save directory of a spectral analysis method
    # NOTE2: unit offset the integer indicating the inverse field range index
    sub_save_directory_abspath = main_save_directory_abspath + os.sep + f'r{range_index+1}'

    # check whether range directory already exists
    # NOTE3: this is a USELESS check because range_index is generated from an enumerate call, and because each call to
    #        spectrum_analysis also creates a new save directory, it is guaranteed that the new range directory does not
    #        exist, yet.
    if os.path.exists(sub_save_directory_abspath):
        print(f'ERROR: can not create sub save directory for field range r{range_index+1}')
        sys.exit(1)

    # create range directory if everything is OK
    os.makedirs(sub_save_directory_abspath)
    return sub_save_directory_abspath


def measurement_control_variable_value(fbasename: str) -> float:
    """
    function for extracting the value of the measurement control variable (temperature T, angle A,
    external field B, ...) [from now on defined as measurement variable value] from base name of
    file.

    :param fbasename: base name of file
    :return: float value of measurement control variable
    """

    # IMPORTANT: pattern1, and therefore pattern2 (it is a subpattern of pattern1) has to be present in base name of
    # file

    # pattern to look for [examples] (...).T40p01K.(...) or (...).A0deg.(...) in base name of file
    pattern1: str = r'\.[A-Z]\d+([a-z]\d+)?([A-Z]|[a-z]+)\.'
    # pattern to look only for the digit part of pattern1 including the decimal part indicated with 'p'
    # [example] for found pattern (...).T40p01K.(...) it extracts (...)40p01(...)
    pattern2: str = r'\d+([a-z]\d+)?'
    matched1: Optional[Match[str]] = re.search(pattern1, fbasename)
    # asserts have to be included for type checking with mypy because Non has no attribute group
    assert matched1 is not None
    matched2: Optional[Match[str]] = re.search(pattern2, matched1.group())
    assert matched2 is not None

    # try to transform found pattern into float by replacing the decimal indicator 'p' with '.'
    try:
        MCV_val: float = float(matched2.group().replace('p', '.'))
    except ValueError:
        print('ERROR: could not assign measurement variable value!')
        sys.exit(1)

    # return value of the measurement control variable
    return MCV_val


def multiple_inverse_field_ranges(method: str = 'original', smallest: float = 0., largest: float = 0., MIN: float = 0.,
                                  MAX: float = 0., step: float = 0.0, halt: Optional[float] = None,
                                  to_larger: bool = True, cutoff: Optional[float] = None) \
                                  -> list[tuple[float, float]]:
    """
    function that creates multiple inverse magnetic field ranges according to certain methods (5 in total)

    :param method: string that indicates 1 out of 5 different methods on how to create inverse magnetic field ranges
    :param smallest: smallest reachable magnetic field value for all input files
    :param largest: largest reachable magnetic field value for all input files
    :param MIN: user input for lower bound of field range (in B-space)
    :param MAX: user input for upper bound of field range (in B-space)
    :param step: user input defining step size when using a shift-method. DEFAULT is 0.0
                 Can be defined in B-space or 1/B-space depending on shift-method.
    :param to_larger: boolean indicating whether shift-method shifts towards larger values (True) or not (False).
                      Can be used in B-space or 1/B-space depending on shift-method.
    :param halt: float indicating where to stop inside [MIN, MAX] for methods where only one boundary is shifted.
    :param cutoff: float indicating the cutoff value for shift-methods when both bounds are shifted.
    :return: list containing all inverse magnetic field ranges with 1 field range being defined as a length-2 tuple.
    """

    if smallest == largest:
        print('ERROR: largest measured field cannot be the the same as smallest!')
        sys.exit(1)
    if MIN == MAX:
        print('ERROR: lower and upper bounds of field range interval cannot be the same!')
        sys.exit(1)
    if method.startswith('shift'):
        if not step:
            print(f'ERROR: shifting not possible with step size {step}!')
            sys.exit(1)
    if method.startswith('shift_M'):
        # assert that halt is defined
        assert halt is not None
        # check that halt falls into valid interval defined by [MIN, MAX]
        if not (MIN <= halt <= MAX):
            print(f'ERROR: halt value {halt} falls outside of valid range!')
            sys.exit(1)
    if method.startswith('shift_both'):
        # assert that cutoff is defined
        assert cutoff is not None
        # check that cutoff falls into valid interval defined by [smallest, largest]
        if not (smallest <= cutoff <= largest):
            print(f'ERROR: cutoff value {cutoff} falls outside of valid range!')
            sys.exit(1)

    # make sure only to take the absolute value of the step size
    step = abs(step)

    # define list that stores all inverse magnetic field ranges
    inv_rngs: list[tuple[float, float]] = []

    # output field range is the same as the input one
    if method == 'original':
        inv_rngs.append(inverse_field_range(MIN, MAX))

    # shift the smaller input magnetic field value towards the larger one by ADDING step
    elif method == 'shift_MIN':
        while MIN < MAX:
            # stop shifting MIN if MIN is lower than halt
            if MIN > halt:
                break
            inv_rngs.append(inverse_field_range(MIN, MAX))
            MIN += step

    # shift the larger input magnetic field value towards the smaller one by SUBTRACTING step
    elif method == 'shift_MAX':
        while MAX > MIN:
            # stop shifting MAX if it is lower than halt
            if MAX < halt:
                break
            inv_rngs.append(inverse_field_range(MIN, MAX))
            MAX -= step

    # shift both input magnetic field values by the same amount step until a certain cutoff is met
    # NOTE1: boolean to_larger True sets the shift direction towards larger values in B-space by ADDING step
    # NOTE2: boolean to_larger False sets the shift direction towards smaller values in B-space by SUBTRACTING step
    elif method == 'shift_both_in_B':
        if to_larger:
            # shift from small B-values towards larger values
            while (MIN >= smallest) and (MAX <= largest) and (MAX <= cutoff):
                inv_rngs.append(inverse_field_range(MIN, MAX))
                MIN += step
                MAX += step
        elif not to_larger:
            # shift from large B-values towards smaller values
            while (MIN >= smallest) and (MAX <= largest) and (MIN >= cutoff):
                inv_rngs.append(inverse_field_range(MIN, MAX))
                MIN -= step
                MAX -= step

    # in 1/B-space shift both input magnetic field values by the same amount step until a certain cutoff is met
    # NOTE3: boolean to_larger True sets the shift direction towards larger values in 1/B-space by ADDING step
    # NOTE4: boolean to_larger False sets the shift direction towards smaller values in 1/B-space by SUBTRACTING step
    elif method == 'shift_both_in_invB':
        if to_larger:
            # shift from small 1/B-values towards larger values
            while ((1/MAX) >= (1/largest)) and ((1/MIN) <= (1/smallest)) and ((1/MIN) <= cutoff):
                inv_rngs.append(inverse_field_range(MIN, MAX))
                MIN = 1 / ((1 / MIN) + step)
                MAX = 1 / ((1 / MAX) + step)
        elif not to_larger:
            # shift from large 1/B-values towards smaller values
            while ((1/MAX) >= (1/largest)) and ((1/MIN) <= (1/smallest)) and ((1/MAX) >= cutoff):
                inv_rngs.append(inverse_field_range(MIN, MAX))
                MIN = 1 / ((1 / MIN) - step)
                MAX = 1 / ((1 / MAX) - step)
    # check that user entered right method name for creating inverse magnetic field ranges
    else:
        print('ERROR: other range creation methods are not implemented!')
        raise ValueError

    return inv_rngs


def number_of_points_for_interpolation(x: np.ndarray, number_of_points: Optional[int] = None, base2: bool = True,
                                       power: int = 0) -> int:
    """
    function calculating the total number of points that define the sampling interval for the x values where the
    interpolator function will be evaluated at. If no user input for number_of_points is given the number of points
    calculated will always match the smallest sampling interval present in the data.

    :param x: np.ndarray containing x-data that will be interpolated
    :param number_of_points: OPTIONAL integer that indicates the num
    :param base2: boolean indicating whether the number of points used for interpolation will be based on a power of 2
                  (True) or not. DEFAULT is True.
    :param power: integer indicating the power (base 2) that will give the total number of points for interpolation
    :return: integer indicating total number of points for interpolation
    """

    # check whether user has entered an integer indicating the number of points he wants for interpolation
    if not number_of_points:
        # get smallest sampling interval available in x-data
        # NOTE: this is ALWAYS true because we are dealing with data that was measured in B-space and then transformed
        #       into 1/B-space resulting in an np.ndarray which has values with ever increasing spacing between data
        #       points in the direction of increasing values in 1/B-space.
        dx: float = x[1] - x[0]

        # calculate the (expected/minimal needed) number of points if the difference between neighbouring data points
        # would be the smallest sampling interval that was just calculated above.
        # NOTE: 1 is added to account for the missing point when taking the difference between two numbers
        number_of_points = int((x[-1] - x[0]) / dx) + 1

        # check whether user wants to make sure that total number of points used for interpolation is a power of 2
        if base2:
            # make sure that power is REALLY an integer
            try:
                power = int(power)
            except ValueError:
                print(ValueError.__doc__)
                print('ERROR: power has to be of type int!')

            # get next power of 2 if user did not specify any power to be used (power=0) OR
            # when a power has been given that would result in less points than based on the smallest sampling interval
            if not power or (2 ** power < number_of_points):
                power = ma.ceil(ma.log(number_of_points, 2))

            # calculate new total number of points for interpolation
            number_of_points = 2 ** power

    # check that number of points is not None
    assert number_of_points

    return number_of_points


def number_of_points_in_total(N: int, base2: bool, sys_ioe: TextIO, power_base2: int = 0) -> int:
    """
    function for calculating the total number of points the data has to have after zero padding.

    :param N: integer indicating initial length of array that will be zero padded
    :param base2: boolean indicating whether the total number of points is a power of 2
    :param sys_ioe: TextIO that is the system default stdout
    :param power_base2: integer value indicating what power of 2 the total number of points will be.
                        DEFAULT is 0.
    :return: integer indicating total number of points the data has to have after zero padding
    """

    # check whether user wants total data array to have arbitrary length by adding an entered value to length of data
    if not base2:

        # ask for user input
        # NOTE: make sure _n_zeros is a positive number
        print('Enter a positive integer indicating number of points that will be padded to the data:')
        _n_zeros: int = abs(user_input(data_type=int,
                                       prompt='n_zeros = ',
                                       sys_ioe=sys_ioe
                                       )
                            )
        n_total: int = N + _n_zeros

        # save user input for logging
        print('\t\t\tEntered #zeros to pad (done only for 1st input file during 1st field range)')
        print(f'\t\t\t\tn_zeros = {_n_zeros}')
        print(f'\t\t\t\tdata_length = {N}')
        print(f'\t\t\t\ttotal data_length = {n_total}\n')

    else:
        # automatically use the power of the next power of 2 (wrt N) if no external input is given (i.e. power_base2=0)
        if not power_base2:
            power_base2 = ma.ceil(ma.log(N, 2)) + 1
        # convert power given in float into int
        elif isinstance(power_base2, float):
            power_base2 = ma.ceil(power_base2)
        # make sure that for float-converted-to-int and valid user input values for power_base2 the resulting total
        # number of points is larger than the input data length N
        while power_base2 <= ma.ceil(ma.log(N, 2)):
            print(f'power={power_base2} for total number of points with zeros padded smaller than/equal to length of'
                  'y-data!')
            power_base2 += 1
            print(f'Adding 1 to raise to power={power_base2}')

        # calculate actual number of zeros based on the valid power and data length N
        n_total = 2**power_base2

    return n_total


def parsing():
    """
    function handling argument parsing

    :return: Namespace containing all parsed arguments
    """

    # https://stackoverflow.com/questions/59178961/how-to-get-file-path-from-user-to-then-pass-into-functions-argparse
    # https://www.golinuxcloud.com/python-argparse/
    # https://docs.python.org/dev/library/argparse.html#the-add-argument-method

    # create parser
    parser = argp.ArgumentParser()

    # add file path as positional argument
    parser.add_argument('json_filepath',
                        help='Absolute path to .json file where all input parameters are declared',
                        type=validate_filepath,
                        metavar='JSON FILEPATH'
                        )

    # add boolean control flag that when disabled does not reroute the standard system I/O
    parser.add_argument('-sioe',
                        '--system_ioe',
                        action='store_true',
                        default=False
                        )

    # return arguments
    return parser.parse_args()


def save_spectrum(freq: np.ndarray, ampl: np.ndarray, invfield_range: tuple[float, float],
                  main_save_dir: str, sub_save_dir: str, in_file_info: dict[str, Any], derivative_order: str,
                  cutoff: Optional[float] = None, normalize_by_max: bool = False, normalize_by_mean: bool = False,
                  multiple_y_columns: bool = False, ext: str = 'dat', comment_floatformat: int = 5) -> int:
    """
    file that saves the calculated spectrum of the spectral analysis.

    :param freq: np.ndarray storing calculated frequencies
    :param ampl: np.ndarray stroing the calculated amplitude
    :param invfield_range: tuple of 2 floats indicating the inverse field range used for spectral analysis
    :param main_save_dir: string indicating the absolute path of the main save directory where all sub save directories
                          (one for each inverse field range) are stored
    :param sub_save_dir: string indicating the absolute path of the directory where all spectral analysis output of 1
                         inverse field range is stored
    :param in_file_info: dictionary of string-Any_type key-value pairs storing all information of 1 input file at a time
    :param derivative_order: string indicating the derivative order used for the just performed spectral analysis
    :param cutoff: Optional float indicating the maximum value up to which frequencies and amplitudes are saved
    :param normalize_by_max: boolean indicating whether to normalize amplitudes by maximum value of FFT amplitude array
    :param normalize_by_mean: boolean indicating whether to normalize amplitudes by mean value of FFT amplitude array
    :param multiple_y_columns: boolean indicating whether output file consists of multiple y-data columns, i.e. gives an
                             y-data column for the FFT amplitude and the normalized FFT amplitude.
    :param ext: string indicating the file format of a output files.
                DEFAULT is 'dat'
    :param comment_floatformat: integer indicating up to which decimal the floats indicating the field range within the
                                comment line
    :return: integer '0' indicating that everything went right
    """

    # check that main-save directory path is contained within sub-save directory path
    if main_save_dir not in sub_save_dir:
        print(f'ERROR: wrong sub-save directory {sub_save_dir}')
        sys.exit(1)

    # create base name of save file (save file base name: sfbasename)
    sfbasename_elements: list[str] = [os.path.basename(main_save_dir),
                                      derivative_order,
                                      in_file_info['MCV_str'],
                                      in_file_info['sample_name'],
                                      in_file_info['original_file_no'],
                                      os.path.basename(sub_save_dir),
                                      ext
                                      ]
    sfbasename: str = '.'.join(filter(None, sfbasename_elements))

    # set cutoff to max value of freq when no user input is available
    if not cutoff:
        cutoff = max(freq)

    # set default value for normalization factor
    norm_factor: float = 1.0
    # normalize by maximum value of amplitude array
    if normalize_by_max:
        print(f'\t\t\t\tmax(ampl) = {max(ampl)}')
        norm_factor = max(ampl) * norm_factor
    # normalize by mean value of amplitude array
    if normalize_by_mean:
        print(f'\t\t\t\tmean(ampl) = {np.mean(ampl)}\n\n')
        norm_factor = np.mean(ampl) * norm_factor

    # define absolute path to save file
    sfpath: str = sub_save_dir + os.sep + sfbasename

    # write to save file
    with open(sfpath, 'w') as sfp:

        # create save file with as output the FFT amplitude AND the normalized FFT amplitude output
        if multiple_y_columns and (normalize_by_max or normalize_by_mean):
            # create file header part
            # write variable names for indicating frequency and amplitude columns
            sfp.write('#freq\tampl\tnorm_ampl\n')

            # write comment line indicating field range and MCV
            sfp.write(f"#{1./invfield_range[1]:.{comment_floatformat}f} - "
                      f"{1./invfield_range[0]:.{comment_floatformat}f} T\t"
                      f"{in_file_info['MCV_value']} {in_file_info['MCV_units']}\t"
                      f"{in_file_info['MCV_value']} {in_file_info['MCV_units']}\n"
                      )

            # save data
            for i in range(0, len(freq)):
                # end data saving when frequency cutoff is exceeded
                if freq[i] > cutoff:
                    break
                sfp.write(f'{freq[i]}\t{ampl[i]}\t{ampl[i]/norm_factor}\n')

        # create save file with as output ONLY the normalized FFT amplitude output
        else:
            # create file header part
            # write variable names for indicating frequency and amplitude columns
            sfp.write('#freq\tampl\n')

            # write comment line indicating field range and MCV
            sfp.write(f"#{1./invfield_range[1]:.{comment_floatformat}f} - "
                      f"{1./invfield_range[0]:.{comment_floatformat}f} T\t"
                      f"{in_file_info['MCV_value']} {in_file_info['MCV_units']}\n"
                      )

            # save data
            for i in range(0, len(freq)):
                # end data saving when frequency cutoff is exceeded
                if freq[i] > cutoff:
                    break
                sfp.write(f'{freq[i]}\t{ampl[i]/norm_factor}\n')

    return 0


def select_data_within_xrange(x: np.ndarray, y: np.ndarray, MIN: float, MAX: float) \
        -> tuple[np.ndarray, np.ndarray]:
    """
    function that selects x- and y-data in a certain value range defined by [MIN, MAX] by using a numpy.ndarray of
    booleans

    :param x: numpy.ndarray containing x-data
    :param y: numpy.ndarray containing y-data
    :param MIN: float indicating the lower bound of the range
    :param MAX: float indicating the upper bound of the range
    :return: tuple of 2 numpy.ndarrays containing the selected x- and y-data values that fall into the range.
    """
    assert MIN < MAX
    # define boolean array with the same length as x.
    # NOTE: by comparing the values within x against MIN/MAX. Either False or True is the result during comparison.
    # https://stackoverflow.com/questions/19984102/select-elements-of-numpy-array-via-boolean-mask-array
    bool_idx_arr = (x >= MIN) * (x <= MAX)

    # return the data points falling into selected range by using the boolean array as boolean index.
    return x[bool_idx_arr], y[bool_idx_arr]


def spectrum_fft(y: np.ndarray, dx: float, onesided: bool = False) -> tuple[np.ndarray, np.ndarray]:
    """
    function calculating the FFT.

    :param y: np.ndarray containin y-data
    :param dx: float indicating sampling interval
    :param onesided: boolean indicating whether only the onesided spectrum is returned (True) (i.e. only positive
                     frequencies).
                     DEFAULT is True.
    :return: tuple containing 2 np.ndarrays indicating the calculated frequencies and FFT amplitudes
    """
    # difference between numpy.fft and scipy.fftpack:
    # https://stackoverflow.com/questions/6363154/what-is-the-difference-between-numpy-fft-and-scipy-fftpack
    # DOCUMENTATION scipy.fftpack
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.fft.fftfreq.html#scipy.fft.fftfreq
    # https://docs.scipy.org/doc/scipy/reference/fft.html

    # NOTE: not using fftpack anymore due to being LEGACY. Switching to scipy.fft
    # https://realpython.com/python-scipy-fft/

    # get length of y-data
    # NOTE: most of the time length n of input array will be a power of 2
    data_len: int = len(y)

    # calculate frequencies of FFT based on the length of the data and the sampling interval
    freq: np.ndarray = scfft.fftfreq(n=data_len,
                                     d=dx
                                     )

    # Calculate normalized amplitude of FFT
    # NOTE: abs() is taken because scfft.fft() output is complex
    # NOTE: dividing by length n of input array gives the normalized amplitudes
    ampl: np.ndarray = abs(scfft.fft(y, n=data_len)) / data_len

    # if onesided is True return only positive frequency spectrum
    if onesided:
        # check whether length of data is a power of 2 or an even number and not 0
        if ((data_len & (data_len - 1)) == 0 or (data_len % 2) == 0) and data_len:
            # if so check that position of the first negative frequency value is right
            assert freq[int(data_len / 2)] < 0
            # only get the positive frequency FFT spectrum
            freq = freq[:int(data_len / 2)]
            ampl = ampl[:int(data_len / 2)]
        # check whether the length of data is odd
        elif (data_len % 2) == 1:
            # if so check that position of the first negative frequency value is right
            assert freq[int((data_len - 1) / 2) + 1] < 0
            # only get the positive frequency FFT spectrum
            freq = freq[:int((data_len - 1) / 2) + 1]
            ampl = ampl[:int((data_len - 1) / 2) + 1]

    return freq, ampl


def spectrum_mem(y: np.ndarray, m_order: Optional[int], opt_method: str, Burg_method: str, regularis: float,
                 early_stop: bool, verbose: bool, dt: float, onesided: bool = False) -> tuple[np.ndarray, np.ndarray]:
    """
    function that initialises the Maximum Entropy Model and returns its spectrum.
    See more information at documentation on 'memspectrum' package.

    :param y: np.ndarray indicating data array
    :param m_order: int Maximum number of recursions for the computation of the  power spectral density.
                    Maximum autoregressive order is p = m-1
                    Default is None, that means m = 2N / log(2N)
    :param opt_method: str Method used to select the best recursive order. The order is chosen minimizing the
                       corresponding method.
                       Available methods are "FPE", "OBD", "CAT", "AIC", "Fixed".
                       If optimisation_method is "Fixed", the autoregressive order is always set to m, without looking
                       for a minimum.
                       Default is "FPE".
    :param Burg_method: str Can be "standard" or "Fast". Selects the algorithm  used to compute the power spectral
                        density with.
                        Default is "Fast"
    :param regularis: float Implement Tikhonov regularisation. Should be a number slightly larger than one.
                      Default is 1, which means no regularisation.
    :param early_stop: boolean Breaks the iteration if there is no new global maximum after 100 iterations.
                       Recommended for every optimisation method but CAT.
                       Has no effect with "Fixed" loss function.
                       Default is True.
    :param verbose: boolean Whether to print the status of the MESA solution.
    :param dt: float indicating sampling rate.
    :param onesided: boolean indicating whether the one sided PSD (only positive frequencies) shall be returned.
                     It has effect only if a frequency array is not given
    :return: tuple of 2 np.ndarrays indicating the calculated frequencies and amplitudes of MESA
    """
    # initialization of Maximum Entropy Spectral Analysis object
    mem = MESA()

    # solve MESA object
    mem.solve(y,
              m=m_order,
              optimisation_method=opt_method,
              method=Burg_method,
              regularisation=regularis,
              early_stop=early_stop,
              verbose=verbose)

    # return calculated frequencies and amplitudes of MESA object
    return mem.spectrum(dt, onesided=onesided)


def take_inverse_of_xdata(x: np.ndarray, y: np.ndarray) -> tuple[np.ndarray, np.ndarray]:
    """
    function that takes the inverse of x-data, and finally making x-data monotonically increasing (transforming y-data
    accordingly).

    :param x: numpy.ndarray containing x-data
    :param y: numpy.ndarray containing y-data
    :return: tuple of 2 numpy.ndarrays containing reversed 1/x- and y-data
    """

    # exclude (x=0, y) data point as the inverse of x=0 is not defined
    if not x[0]:
        x = x[1:]
        y = y[1:]

    # take the inverse of x-data
    xinv: np.ndarray = 1 / x

    # reverse both arrays to make the x-data monotonically increasing again and match the y-data
    xinv = xinv[::-1]
    yinv: np.ndarray = y[::-1]

    return xinv, yinv


def user_input(prompt: str, data_type, sys_ioe: TextIO):
    """
    function for preventing user to enter wrong value for a parameter.

    :param prompt: string message that is printed to terminal to inform user what parameter has to be set.
    :param data_type: user defined data type
    :param sys_ioe: TextIO that is the system default stdout
    :return: parameter of type data_type
    """

    def _user_input(_sys_ioe: TextIO, _prompt: str = None) -> str:
        # https://stackoverflow.com/questions/21202403/
        # how-to-redirect-the-raw-input-to-stderr-and-not-stdout/21203064#21203064
        if _prompt:
            _sys_ioe.write(str(_prompt))
        return input()

    # assign default value of None to parameter
    parameter = None

    # enter 'infinite' while loop to be able to keep prompting user for correct input
    while True:

        # try assigning value to parameter
        try:
            parameter = data_type(_user_input(_sys_ioe=sys_ioe, _prompt=prompt))
            break

        # catch exceptions to prompt user again
        except Exception as e:
            print(e.__doc__)
            print('WARNING: initialization of parameter failed!')
            print('Try again...')
            continue

    # assert that parameter is not None
    assert parameter is not None

    # when successful return parameter
    return parameter


def validate_filepath(fpath: str) -> str:
    """
    function that checks whether a file exists

    :param fpath: string indicating the absolute path of a file
    :return: string indicating the absolute path of a file
    """

    # https://stackoverflow.com/questions/59178961/how-to-get-file-path-from-user-to-then-pass-into-functions-argparse

    # check whether the file with given path exists
    if not os.path.exists(fpath):

        # raise error if file does not exist
        raise argp.ArgumentTypeError(f'{fpath} does not exist.')

    return fpath


def window_func_settings(index: int, sys_ioe: TextIO) -> tuple:
    """
    function for declaring the settings needed to initialize a window function before using FFT on data.

    :param index: integer indicating the index in the window names dictionary w_names of wanted window function
    :param sys_ioe: TextIO that is the system default stdout
    :return: tuple containing name of method by which window function is calculated and arguments of window function
    """

    # check that user selects a window function method
    if not index:
        print('ERROR: indicate which window function will be used!')
        sys.exit(1)

    # dictionary with all window function names
    w_names: dict[int, str] = {1: 'boxcar',
                               2: 'triang',
                               3: 'blackman',
                               4: 'hamming',
                               5: 'hann',
                               6: 'bartlett',
                               7: 'flattop',
                               8: 'parzen',
                               9: 'bohman',
                               10: 'blackmanharris',
                               11: 'nuttall',
                               12: 'barthann',
                               13: 'cosine',
                               14: 'exponential',
                               15: 'tukey',
                               16: 'taylor',
                               17: 'kaiser',
                               18: 'gaussian',
                               19: 'general_cosine',
                               20: 'general_gaussian',
                               21: 'general_hamming',
                               22: 'dpss',
                               23: 'chebwin'
                               }

    # define tuple containing settings (name, ect.) of window function
    w_settings: tuple = (w_names[index],)

    # initialize window function parameters if necessary
    if 17 <= index <= 23:
        # log name of window function that uses extra parameters
        print(f'\twindow function: {w_settings[0]}')

        # activate system default stdout
        # NOTE: system default stdout is not active beforehand as everything is logged from the start of 'main()'
        # sys.stdout = standard_out

        # KAISER
        if index == 17:
            beta: float = user_input(data_type=float,
                                     prompt='beta = ',
                                     sys_ioe=sys_ioe
                                     )
            w_settings += (beta,)

            # switch back to user defined stdout used for logging
            # sys.stdout = user_out

            # save user input for logging
            print(f'\t\tbeta = {beta}')

        # GAUSSIAN
        elif index == 18:
            std: float = user_input(data_type=float,
                                    prompt='std = ',
                                    sys_ioe=sys_ioe
                                    )
            w_settings += (std,)

            # switch back to user defined stdout used for logging
            # sys.stdout = user_out

            # save user input for logging
            print(f'\t\tstd = {std}')

        # GENERAL_COSINE
        elif index == 19:
            HFT90D: list[float] = [1., 1.942604, 1.340318, 0.440811, 0.043097]
            w_settings += (HFT90D,)

            # switch back to user defined stdout used for logging
            # sys.stdout = user_out

            # save user input for logging
            print(f'\t\tHFT90D = {HFT90D}')

        # GENERAL_GAUSSIAN
        elif index == 20:
            p: float = user_input(data_type=float,
                                  prompt='p = ',
                                  sys_ioe=sys_ioe
                                  )
            sig: float = user_input(data_type=float,
                                    prompt='sig = ',
                                    sys_ioe=sys_ioe
                                    )
            w_settings += (p, sig,)

            # switch back to user defined stdout used for logging
            # sys.stdout = user_out

            # save user input for logging
            print(f'\t\tp = {p}')
            print(f'\t\tsig = {sig}')

        # GENERAL_HAMMING
        elif index == 21:
            alpha: float = user_input(data_type=float,
                                      prompt='alpha = ',
                                      sys_ioe=sys_ioe
                                      )
            w_settings += (alpha,)

            # switch back to user defined stdout used for logging
            # sys.stdout = user_out

            # save user input for logging
            print(f'\t\talpha = {alpha}')

        # DPSS
        elif index == 22:
            NW: float = user_input(data_type=float,
                                   prompt='NW = ',
                                   sys_ioe=sys_ioe
                                   )
            w_settings += (NW,)

            # switch back to user defined stdout used for logging
            # sys.stdout = user_out

            # save user input for logging
            print(f'\t\tNW = {NW}')

        # CHEBWIN
        elif index == 23:
            at: float = user_input(data_type=float,
                                   prompt='at = ',
                                   sys_ioe=sys_ioe
                                   )
            w_settings += (at,)

            # switch back to user defined stdout used for logging
            # sys.stdout = user_out

            # save user input for logging
            print(f'\t\tat = {at}')

    # return window function settings
    return w_settings


def zero_padding(y: np.ndarray, n_total: Optional[int], n_zeros_before: int = 0) -> np.ndarray:
    """
    function that zero pads y-data.

    :param y: np.ndarray of input y-data
    :param n_total: integer indicating the total number of points the data has to have after zero padding
    :param n_zeros_before: integer indicating the number of zeros that will be added in front of first y-data element
    :return: np.ndarray of zero padded y-data
    """

    # check that total length is of type integer
    if not isinstance(n_total, int):
        print('ERROR: total number of points is not of type int!')
        sys.exit(1)

    # catch case of not wanting to zero pad, i.e. n_total = 0
    if n_total == 0:
        # set equal to length of data because then the number of zeros after is always 0
        n_total = len(y)

    # initialize an integer indicating the number of zeros padded after the last data point
    # NOTE: first disregard the number of zeros added in front (zeropad_n_before) of the first data point
    n_zeros_after: int = n_total - len(y)

    # check whether user also wants to zero pad before the first data point ONLY if n_zeros_after is NOT 0
    if n_zeros_before and n_zeros_after:
        # check whether n_zeros_before does not exceed n_zeros_after as there are only a total of n_zero (currently the
        # value of n_zeros_after) zeros
        if n_zeros_before <= n_zeros_after:
            # recalculate n_zeros_after
            n_zeros_after = n_zeros_after - n_zeros_before
        # set n_zeros_before to 0 if it is larger than
        else:
            print('WARNING: n_zeros_before must not exceed value of after!\n'
                  'Setting "n_zeros_before" to 0')
            n_zeros_before = 0

    # perform and return zero padding of y-data
    return np.pad(y, (n_zeros_before, n_zeros_after))
# ____________________________________________________________________________________________________________________ #

# ____________________________________________________________________________________________________________________ #


def main(parameters, sys_ioe_active: bool) -> int:
    """
    function that performs initialisation off all needed parameters, file loading/checking and all of the needed data
    transformation so that spectral analyses can be performed and saved

    :param parameters: dictionary obtained from json file that stores all external input parameters defined by user
    :param sys_ioe_active: boolean that indicates whether standard system ioe (input/output/error) is active
    :return: integer equal to '0' if function worked properly
    """

    # -------------------------------------------------------------------------
    #
    #
    #
    #############################
    # CREATE GENERAL LOG STREAM #
    #############################

    # save a copy of the standard sys.stdout/sys.stderr
    sys_stdout: TextIO = sys.stdout
    sys_stderr: TextIO = sys.stderr

    # define a separate text stream for logging general output
    general_log_stream_stdout = io.StringIO()

    # tee sys.stdout
    # NOTE: type ignore due to
    sys.stdout = Tee(user_ioe=general_log_stream_stdout, sys_active=sys_ioe_active, sys_ioe=sys_stdout)  # type: ignore

    #
    #
    #
    # -------------------------------------------------------------------------

    print('Initializing parameters...\n')

    # -------------------------------------------------------------------------
    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # -------------------------------USER INPUT NEEDED-------------------------
    #                |                      |                    |
    #                |                      |                    |
    #                |                      |                    |
    #                |                      |                    |
    #                |                      |                    |
    #                v                      v                    v
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # -------------------------------------------------------------------------
    #
    #
    #
    #
    #################################
    # CONTROL over CALCULATION FLOW #
    #################################

    # bool whether or not to check the input files
    # NOTE: !!!input files should be checked at least ONCE!!!
    CFlow_check_input_files: bool = parameters['CFlow_check_input_files']

    # bool dictionary for selecting which derivatives data will be calculated during interpolation step
    # NOTE: only set one of both to True as 'd2' also calculates 'd1' internally
    # NOTE: 'd0', i.e. the original data is always present
    CFlow_calculate_deriv_orders: dict[str, bool] = parameters['CFlow_calculate_deriv_orders']

    # bool dictionary for excluding certain derivative orders when 'deriv_order_sel_bool = True'
    # NOTE: do not exclude 'd0' when both d1_bool and d2_bool are False
    CFlow_exclude_deriv_orders: dict[str, bool] = parameters['CFlow_exclude_deriv_orders']
    #
    #
    #
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #
    #
    #
    ############################
    # CONTROL over INPUT FILES #
    ############################

    # set resource directory for input files
    input_fresource_dir: str = parameters['input_fresource_dir']

    # [OPTIONAL] define list of input file base names that are the ONLY ones to be analyzed
    # NOTE: an empty list means that all input files in the resource directory will be analyzed
    input_files_to_analyze: list[str] = parameters['input_files_to_analyze']

    # set delimiter
    input_delim: str = parameters['input_delim']

    # set input file extension
    # NOTE: DEFAULT is 'dat'
    input_file_ext: str = parameters['input_file_ext']

    # set number of rows to skip in input file
    input_skiprows: int = parameters['input_skiprows']

    # tuple defining which two columns will be used for analysis
    # NOTE: only 2 columns are allowed as input with DEFAULT being indices 0, 1
    input_use_columns: tuple[int, int] = (parameters['input_use_columns_x'],
                                          parameters['input_use_columns_y']
                                          )

    # set integer indicating in which line the names of all variables measured are stored
    input_var_names_line_no: int = parameters['input_var_names_line_no']

    # set data type of data that will be loaded
    if parameters['input_data_type'] == 'float':
        input_data_type = float
    else:
        raise TypeError('Data type of input needs to be float')

    # set measurement variable that was used for control, will be used in info tuple 'finfo' for each input file
    # Measurement Control Variable (MCV)
    input_MCV_name: str = parameters['input_MCV_name']
    input_MCV_units: str = parameters['input_MCV_units']

    #
    #
    #
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #
    #
    #
    ###############################
    # CONTROL over FIELD RANGE(S) #
    ###############################

    # define dictionary containing all different methods for setting field ranges
    #   1: 'original'
    #   2: 'shift_Min'
    #   3: 'shift_Max'
    #   4: 'shift_both_in_B'
    #   5: 'shift_both_in_invB'
    FieldRng_methods_dict: dict[int, str] = {1: 'original',
                                             2: 'shift_MIN',
                                             3: 'shift_MAX',
                                             4: 'shift_both_in_B',
                                             5: 'shift_both_in_invB'
                                             }
    # set index of method for setting field ranges
    FieldRng_methods_idx: int = parameters['FieldRng_methods_idx']
    # get range method
    FieldRng_method: str = FieldRng_methods_dict[FieldRng_methods_idx]

    # set initial field range [Bmin, Bmax]
    FieldRng_Bmin: float = float_or_evaluate_str_expression(parameters['FieldRng_Bmin'])
    FieldRng_Bmax: float = float_or_evaluate_str_expression(parameters['FieldRng_Bmax'])

    # set value for step size
    # NOTE: step is only needed for methods 'shift_MIN', 'shift_MAX', 'shift_both_in_B', 'shift_both_in_invB'
    FieldRng_Bstep: float = float_or_evaluate_str_expression(parameters['FieldRng_Bstep'])

    # set halt value for stopping in somewhere in between when shifting either only Bmin or Bmax (methods 2 and 3)
    FieldRng_shiftMINMAX_halt: float = float_or_evaluate_str_expression(parameters['FieldRng_shiftMINMAX_halt'])

    # set boolean indicating in which direction range window [Bmin, Bmax] (or [1/Bmax, 1/Bmin]) is shifted
    # NOTE: this boolean is only available for all 'shift_both_...' range methods
    # NOTE: True means range window is shifted from small towards large values
    #       False means range window is shifted from large towards small values
    FieldRng_shift_to_larger: bool = parameters['FieldRng_shift_to_larger']

    # set a cutoff field value field_cutoff
    # NOTE: field_cutoff is only needed for methods 'shift_both_in_B' and 'shift_both_in_invB'
    FieldRng_cutoff: float = float_or_evaluate_str_expression(parameters['FieldRng_cutoff'])

    #
    #
    #
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #
    #
    #
    ##############################
    # CONTROL over INTERPOLATION #
    ##############################

    # define dictionary containing all different methods for interpolation
    #   1: 'linear'
    # TODO: start altering this method in order to incorporate nonuniformly sampled ffts (pynufft)
    interpol_methods_dict: dict[int, str] = {1: 'linear'
                                             }

    # set index of method for setting field ranges
    interpol_methods_idx: int = parameters['interpol_methods_idx']

    # set interpolation method
    interpol_method: str = interpol_methods_dict[interpol_methods_idx]

    # OPTIONAL manually set integer indicating how many points are used for interpolation
    # NOTE: is value is None then the number of points will automatically be calculated to match the samllest sampling
    #       interval present in data
    interpol_n_manual: Optional[int] = parameters['interpol_n_manual']

    # set boolean indicating that data should be interpolated such that total number of points is a power of 2
    interpol_base2_bool: bool = parameters['interpol_base2_bool']

    # OPTIONAL: set power (used with base 2) for number of points used in interpolation
    # NOTE: setting it to 0 here let's program find the next number of points (base2) automatically
    interpol_power_base2: int = parameters['interpol_power_base2']

    #
    #
    #
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #
    #
    #
    #############################
    # CONTROL over ZERO PADDING #
    #############################

    # bool whether zero padding is wanted
    zeropad_bool: bool = parameters['zeropad_bool']

    # bool whether to use powers of 2 for total number of points [= data points + zeros padded]
    zeropad_base2_bool: bool = parameters['zeropad_base2_bool']

    # set power (used with base 2) calculating total length of data array [= length interpolated data + padded zeros]
    # NOTE: if base_2_bool is False this value will not be used
    zeropad_power_base2: int = parameters['zeropad_power_base2']

    # set number of zeros padded before first data point
    # NOTE: only valid if user wants to zero pad
    if zeropad_bool:
        zeropad_n_before: int = parameters['zeropad_n_before']
    else:
        zeropad_n_before = 0

    # initialize total number points that data has to have after zero padding as Optional integer
    # NOTE: Optional is only chosen to ensure mypy type safe, but it will get an integer assigned for sure
    # !!!do NOT INDICATE how many 0s you want to have here, this will be done in the 1st loop below during runtime!!!
    zeropad_n_total: Optional[int] = None

    #
    #
    #
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #
    #
    #
    ##################################
    # CONTROL over SPECTRAL ANALYSES #
    ##################################
    # -------------------------------------------------------------------------
    #
    #
    #
    ####################
    # CONTROL over FFT #
    ####################

    # bool for calculating FFT spectrum
    fft_bool: bool = parameters['fft_bool']

    # set FFT output to onesided, positive frequencies only
    fft_onesided: bool = parameters['fft_onesided']

    #########################################
    # CONTROL over WINDOW FUNCTIONS for FFT #
    #########################################

    # Available window functions
    #    1: 'boxcar'      6: 'bartlett'         11: 'nuttall'       16: 'taylor'            21: 'general_hamming'
    #    2: 'triang'      7: 'flattop'          12: 'barthann'      17: 'kaiser'            22: 'dpss'
    #    3: 'blackman'    8: 'parzen'           13: 'cosine'        18: 'gaussian'          23: 'chebwin'
    #    4: 'hamming'     9: 'bohman'           14: 'exponential'   19: 'general_cosine'
    #    5: 'hann'       10: 'blackmanharris'   15: 'tukey'         20: 'general_gaussian'
    # set window function by choosing an index from above
    # NOTE: 5: 'hann' is default
    window_idx: int = parameters['window_idx']
    window_settings: tuple = window_func_settings(index=window_idx,
                                                  sys_ioe=sys_stdout
                                                  )

    # set boolean that controls periodic/symmetric behaviour of window function
    # NOTE: True lets window function be periodic for use in FFT
    #       False lets window function be symmetric for use in filtering
    window_periodic: bool = parameters['window_periodic']

    # -------------------------------------------------------------------------
    #
    #
    #
    ####################
    # CONTROL over MEM #
    ####################

    # bool for calculating MEM spectrum
    mem_bool: bool = parameters['mem_bool']

    # set initial recursive order
    # NOTE: DEFAULT is m = int(2*N/np.log(2.*N)) with np.log being the natural log, N length of data array, if None upon
    #       initialization
    mem_recur_order_init: Optional[int] = parameters['mem_recur_order_init']

    # set optimisation method (loss function) to choose the best recursive order for Burg's method
    # NOTE: DEFAULT is FPE
    mem_opt_methods: dict[int, str] = {1: 'FPE',
                                       2: 'CAT',
                                       3: 'OBD',
                                       4: 'AIC',
                                       5: 'LL',
                                       6: 'Fixed'
                                       }
    mem_opt_methods_idx: int = parameters['mem_opt_methods_idx']
    mem_opt_method: str = mem_opt_methods[mem_opt_methods_idx]

    # set algorithm for Burg's method
    # NOTE: DEFAULT is Fast
    mem_Burg_methods: dict[int, str] = {1: 'Fast',
                                        2: 'standard'
                                        }
    mem_Burg_methods_idx: int = parameters['mem_Burg_methods_idx']
    mem_Burg_method: str = mem_Burg_methods[mem_Burg_methods_idx]

    # set value for Tikhonov regularisation (should be >= 1. , with no regularisation for = 1.)
    # NOTE: DEFAULT is 1.
    mem_Tikh_reg: float = parameters['mem_Tikh_reg']

    # set boolean for stopping MEM early if no new maximum is encountered after 100 iterations
    # NOTE: DEFAULT is True, but not recommended for 'CAT' optimisation method,
    #       has no effect for 'Fixed' optimisation
    mem_early_stop: bool = parameters['mem_early_stop']

    # set boolean enabling status display of MESA (MEM) solution
    # NOTE: DEFAULT is False
    mem_verbose: bool = parameters['mem_verbose']

    # set MEM output to onesided, positive frequencies only
    mem_onesided: bool = parameters['mem_onesided']

    #
    #
    #
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #
    #
    #
    #############################
    # CONTROL over OUTPUT FILES #
    #############################

    # set name(s) for core basename of save directory (separate ones for different spectral methods) based on if they
    # will be calculated
    output_save_dir_core_basenames: dict[str, bool] = {'SA_FFT': fft_bool,
                                                       'SA_MEM': mem_bool
                                                       }

    # set output file extension
    # NOTE: DEFAULT is 'dat'
    output_file_ext: str = parameters['output_file_ext']

    # set cutoff value for saving
    output_freq_cutoff: float = parameters['output_freq_cutoff']

    # set spectrum normalization
    output_norm_by_max: bool = parameters['output_norm_by_max']
    output_norm_by_mean: bool = parameters['output_norm_by_mean']

    # set status for multiple y-data columns in output
    output_multiple_y_cols: bool = parameters['output_multiple_y_cols']

    # set float formatter for floats indicating the field range in comment line
    output_comment_floatformat: int = parameters['output_comment_floatformat']

    #
    #
    #
    # -------------------------------------------------------------------------
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    #                ^                      ^                    ^
    #                |                      |                    |
    #                |                      |                    |
    #                |                      |                    |
    #                |                      |                    |
    #                |                      |                    |
    # -------------------------------USER INPUT NEEDED-------------------------
    # //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #
    #
    #
    ##############################################
    # Perform SANITY CHECKS for INPUT PARAMETERS #
    ##############################################
    print('\tChecking input parameters...\n')

    check_calculate_derivative_orders(calculate=CFlow_calculate_deriv_orders)

    check_exclusion_derivative_orders(calculate=CFlow_calculate_deriv_orders,
                                      exclude=CFlow_exclude_deriv_orders
                                      )

    check_spectral_analysis_bools(fft=fft_bool,
                                  mem=mem_bool
                                  )
    #
    #
    #
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #
    #
    #
    ######################################################
    # Perform SCREENING of INPUT FILES used for ANALYSIS #
    ######################################################
    print('Getting input files...\n')

    # get list of base name of each input file
    input_fbasenames: list[str] = get_file_basenames(rsrc_dir=input_fresource_dir,
                                                     ext=input_file_ext
                                                     )

    # sort file base names according to value of the measurement variable that is standard included in file  base name
    input_fbasenames = sorted(input_fbasenames,
                              key=measurement_control_variable_value
                              )

    # [OPTIONAL] screen input files for faulty input and rewrite files in right input format
    if CFlow_check_input_files:

        print('\tChecking input files...\n')

        check_files_for_data_loading(rsrc_dir=input_fresource_dir,
                                     fbasenames=input_fbasenames,
                                     delimiter=input_delim,
                                     skiprows=input_skiprows,
                                     usecols=input_use_columns,
                                     data_type=input_data_type
                                     )

    print('Creating info library for input files...\n')

    # create a library dictionary that stores 'input file base name' - 'input file info' as key-value pairs
    # NOTE : input file info itself is also a dictionary
    Library_input_file_info: dict[str, dict[str, Any]] = {}

    # loop over all input files
    for input_fbasename in input_fbasenames:
        # create file info for input file being handled at the moment
        # NOTE: each input file gets its own file info dictionary created
        Library_input_file_info.update({input_fbasename: create_file_info(rsrc_dir=input_fresource_dir,
                                                                          fbasename=input_fbasename,
                                                                          delimiter=input_delim,
                                                                          usecols=input_use_columns,
                                                                          var_names_line=input_var_names_line_no,
                                                                          MCV_name=input_MCV_name,
                                                                          MCV_units=input_MCV_units
                                                                          )
                                        }
                                       )

    #
    #
    #
    # -------------------------------------------------------------------------

    #############################
    # ACTUAL MAIN ANALYSIS CODE #
    #############################

    print('Setting up spectral analysis\n')

    print('\tCreating main save directories...\n')

    # create save directories
    output_main_save_dirs: list[str] = make_main_save_directories(save_directories_corebasenames=
                                                                  output_save_dir_core_basenames,
                                                                  rsrc_dir=input_fresource_dir
                                                                  )

    print('\tSearching for min/max x-values reachable for all files...\n')

    # return reachable smallest and largest measured x-data values inside a tuple
    reachable_extrema: tuple = get_smallest_and_largest_x(rsrc_dir=input_fresource_dir,
                                                          fbasenames=input_fbasenames,
                                                          delimiter=input_delim,
                                                          skiprows=input_skiprows,
                                                          usecols=input_use_columns,
                                                          data_type=input_data_type
                                                          )

    # extract lower and upper bounds
    B_smallest, B_largest = reachable_extrema

    # reset user-set field range boundaries if necessary
    FieldRng_Bmin, FieldRng_Bmax = check_boundaries_x(smallest=B_smallest,
                                                      largest=B_largest,
                                                      MIN=FieldRng_Bmin,
                                                      MAX=FieldRng_Bmax
                                                      )

    # set keyword arguments for creation of field ranges
    kwargs_fieldrange: dict[str, Any] = {'method': FieldRng_method,
                                         'smallest': B_smallest,
                                         'largest': B_largest,
                                         'MIN': FieldRng_Bmin,
                                         'MAX': FieldRng_Bmax,
                                         'step': FieldRng_Bstep,
                                         'halt': FieldRng_shiftMINMAX_halt,
                                         'to_larger': FieldRng_shift_to_larger,
                                         'cutoff': FieldRng_cutoff
                                         }

    print('\tCreating inverse field ranges...\n')

    # create (multiple) inverse field range(s)
    inverse_field_ranges: list[tuple[float, float]] = multiple_inverse_field_ranges(**kwargs_fieldrange)

    # -------------------------------------------------------------------------
    #
    #
    #
    ########################################
    # CREATE GENERAL SETTINGS DICTIONARIES #
    ########################################

    Settings_Input: dict[str, Any] = {'Library_input_file_info': Library_input_file_info,
                                      'input_fresource_dir': input_fresource_dir,
                                      'input_fbasenames': input_fbasenames,
                                      'input_files_to_analyze': input_files_to_analyze,
                                      'input_var_names_line_no': input_var_names_line_no,
                                      'input_use_columns': input_use_columns,
                                      'input_file_ext': input_file_ext,
                                      'input_skiprows': input_skiprows,
                                      'input_delim': input_delim,
                                      'input_data_type': input_data_type,
                                      'CFlow_check_input_files': CFlow_check_input_files
                                      }

    Settings_Output: dict[str, Any] = {'output_file_ext': output_file_ext,
                                       'output_freq_cutoff': output_freq_cutoff,
                                       'output_norm_by_max': output_norm_by_max,
                                       'output_norm_by_mean': output_norm_by_mean,
                                       'output_multiple_y_cols': output_multiple_y_cols,
                                       'output_comment_floatformat': output_comment_floatformat
                                       }

    Settings_InverseFieldRange: dict[str, Any] = {'FieldRng_method': FieldRng_method,
                                                  'FieldRng_methods_idx': FieldRng_methods_idx,
                                                  'B_smallest': B_smallest,
                                                  'B_largest': B_largest,
                                                  'FieldRng_Bmin': FieldRng_Bmin,
                                                  'FieldRng_Bmax': FieldRng_Bmax,
                                                  'FieldRng_Bstep': FieldRng_Bstep,
                                                  'FieldRng_shiftMINMAX_halt': FieldRng_shiftMINMAX_halt,
                                                  'FieldRng_shift_to_larger': FieldRng_shift_to_larger,
                                                  'FieldRng_cutoff': FieldRng_cutoff
                                                  }

    Settings_Interpolation: dict[str, Any] = {'interpol_method': interpol_method,
                                              'interpol_methods_idx': interpol_methods_idx,
                                              'interpol_n_manual': interpol_n_manual,
                                              'interpol_base2_bool': interpol_base2_bool,
                                              'interpol_power_base2': interpol_power_base2,
                                              'CFlow_calculate_deriv_orders': CFlow_calculate_deriv_orders,
                                              'CFlow_exclude_deriv_orders': CFlow_exclude_deriv_orders
                                              }

    Settings_ZeroPadding: dict[str, Any] = {'zeropad_bool': zeropad_bool,
                                            'zeropad_base2_bool': zeropad_base2_bool,
                                            'zeropad_power_base2': zeropad_power_base2,
                                            'zeropad_n_before': zeropad_n_before
                                            }

    Settings_FFT: dict[str, Any] = {'fft_bool': fft_bool,
                                    'window_settings': window_settings,
                                    'window_idx': window_idx,
                                    'window_periodic': window_periodic,
                                    'fft_onesided': fft_onesided
                                    }

    Settings_MEM: dict[str, Any] = {'mem_bool': mem_bool,
                                    'mem_opt_method': mem_opt_method,
                                    'mem_opt_methods_idx': mem_opt_methods_idx,
                                    'mem_Burg_method': mem_Burg_method,
                                    'mem_Burg_methods_idx': mem_Burg_methods_idx,
                                    'mem_Tikh_reg': mem_Tikh_reg,
                                    'mem_early_stop': mem_early_stop,
                                    'mem_verbose': mem_verbose,
                                    'mem_onesided': mem_onesided,
                                    'mem_recur_order_init': mem_recur_order_init
                                    }

    #
    #
    #
    # -------------------------------------------------------------------------

    # loop over all types of spectral analyses
    # NOTE: per runtime there is only 1 new save directory created per spectral analysis method
    for output_main_save_dir in output_main_save_dirs:

        # -------------------------------------------------------------------------
        #
        #
        #
        #####################################################
        # CREATE DOCUMENTATION for SPECTRAL ANALYSIS METHOD #
        #####################################################

        # create a workbook that documents all settings (general and field range specific)
        Documentation_wb = opxl.Workbook()

        # ALWAYS remove default worksheet
        Documentation_wb.remove(Documentation_wb.active)

        #
        #
        #
        # -------------------------------------------------------------------------

        # -------------------------------------------------------------------------
        #
        #
        #
        #############################################################
        # UPDATE GENERAL DOCUMENTATION for SPECTRAL ANALYSIS METHOD #
        #############################################################

        # update documentation for general settings
        documentation_update_settings_general(documentation=Documentation_wb,
                                              settings_input=Settings_Input,
                                              settings_output=Settings_Output,
                                              settings_fieldrange=Settings_InverseFieldRange,
                                              settings_interpolation=Settings_Interpolation,
                                              settings_zeropadding=Settings_ZeroPadding,
                                              settings_fft=Settings_FFT,
                                              settings_mem=Settings_MEM
                                              )

        #
        #
        #
        # -------------------------------------------------------------------------

        # -------------------------------------------------------------------------
        #
        #
        #
        ##############################
        # CREATE SPECTRAL LOG STREAM #
        ##############################

        # NOTE: Here the GENERAL LOG STREAM is rerouted into the SPECTRAL LOG STREAM

        # define another text stream for logging spectral analysis specific output
        spectral_log_stream_stdout = io.StringIO()

        # tee sys.stdout again to save different spectral analysis info separately
        sys.stdout = Tee(user_ioe=spectral_log_stream_stdout, sys_active=sys_ioe_active,  # type: ignore
                         sys_ioe=sys_stdout)  # type: ignore

        #
        #
        #
        # -------------------------------------------------------------------------
        print(f'\nStarting spectral analysis for {os.path.basename(output_main_save_dir)}\n')

        # create dictionary that stores all field range specific settings for each field range
        Settings_RangeSpecific: dict[str, Any] = {}

        temp_fieldrange = None

        # loop over all ranges
        for inv_field_rng_idx, inv_field_rng in enumerate(inverse_field_ranges):

            # initialize a temporary field range control parameter
            if not temp_fieldrange:
                temp_fieldrange = inv_field_rng

            print(f'\tConsidering range r{inv_field_rng_idx + 1} ({1./inv_field_rng[-1]} - {1./inv_field_rng[0]} T)\n')

            # create new settings dictionary for active range
            Settings_RangeSpecific.update({f'r{inv_field_rng_idx + 1}': {}})

            # save field range interval for active range
            Settings_RangeSpecific[f'r{inv_field_rng_idx + 1}'].update({f'r{inv_field_rng_idx + 1}_interval':
                                                                        inv_field_rng
                                                                        }
                                                                       )

            # create sub-directories for saving each field range separately
            output_sub_save_dir: str = make_sub_save_directory(range_index=inv_field_rng_idx,
                                                               main_save_directory_abspath=output_main_save_dir
                                                               )
            print(f'\t\tCreated sub save directory {os.path.basename(output_sub_save_dir)}\n')

            # set initial value for the number of points used for interpolation
            # regulates irregular datasets [datasets with non-representative first data files]
            # NOTE: takes on integer value of number of points used for interpolation of first data file
            # !!!DO NOT ALTER THIS VALUE TO ANYTHING ELSE THAN 0!!!
            interpol_n_check: int = 0

            # create for each range a library dictionary that stores for each input file that is set to be analyzed a
            # dictionary that holds all information on that input file's range specific settings
            Library_file_range_specific_info: dict[str, dict[str, Any]] = {}

            # loop over all files
            for input_fbasename in input_fbasenames:

                # skip files for whom it is not EXPLICITLY stated that they will be analyzed
                # NOTE: no EXPLICIT statement at all means all files will be analyzed
                if input_files_to_analyze and (input_fbasename not in input_files_to_analyze):
                    continue

                # create a dictionary that stores all range specific settings of active to be analyzed input file
                file_range_specific_info: dict[str, Any] = {}

                print(f'\t\tAnalysing file {input_fbasename}\n')

                # first, define the file path from the file resource directory and the file base name
                input_fpath = input_fresource_dir + os.sep + input_fbasename

                # get the data from the file
                x, y = load_data(fpath=input_fpath,
                                 delimiter=input_delim,
                                 skiprows=input_skiprows,
                                 usecols=input_use_columns
                                 )

                # bin the acquired data by using 'numpy-indexed' module
                x, y = npidx.group_by(x).mean(y)

                # take the inverse of the data
                xinv, yinv = take_inverse_of_xdata(x=x,
                                                   y=y
                                                   )

                # select data within inverse field range
                xinv_sel, yinv_sel = select_data_within_xrange(x=xinv,
                                                               y=yinv,
                                                               MIN=inv_field_rng[0],
                                                               MAX=inv_field_rng[1]
                                                               )

                # get info on field range selected data bounds, data length, minimal sampling rate
                file_range_specific_info.update({'xinv_sel_min': xinv_sel[0],
                                                 'xinv_sel_max': xinv_sel[-1],
                                                 'xinv_sel_len': len(xinv_sel),
                                                 'yinv_sel_len': len(yinv_sel),
                                                 'dxinv_sel_sr_min': xinv_sel[1] - xinv_sel[0]
                                                 }
                                                )

                # get total number of points for interpolation
                interpol_n: int = number_of_points_for_interpolation(x=xinv_sel,
                                                                     number_of_points=interpol_n_manual,
                                                                     base2=interpol_base2_bool,
                                                                     power=interpol_power_base2
                                                                     )

                # based on interpol_n define the x values where interpolator will be evaluated at
                xinv_values_intpl, dxinv_intpl = interpolator_xvalues(x=xinv_sel,
                                                                      number_of_points=interpol_n
                                                                      )
                assert len(xinv_values_intpl) == interpol_n

                # set the total number of points equal for all files after having handled the first file
                if not interpol_n_check:
                    interpol_n_check = len(xinv_values_intpl)

                # for every following file control whether points were spaced in roughly the same way as for the
                # first file
                else:

                    # compare total number of points for interpolation of active file to that of the first file
                    if len(xinv_values_intpl) < interpol_n_check or len(xinv_values_intpl) > interpol_n_check:

                        # if the number of points is smaller/larger than first correct x values for interpolation
                        # and the corresponding sampling interval
                        xinv_values_intpl, dxinv_intpl = correct_interpolator_xvalues(xinv_values_intpl,
                                                                                      N=interpol_n_check
                                                                                      )

                # check that the length of the np.ndarray is now the same for each file
                assert len(xinv_values_intpl) == interpol_n_check

                # get info on interpolation data bounds, data length, sampling rate
                file_range_specific_info.update({'xinv_values_intpl_min': xinv_values_intpl[0],
                                                 'xinv_values_intpl_max': xinv_values_intpl[-1],
                                                 'xinv_values_intpl_len': len(xinv_values_intpl),
                                                 'interpol_n_check': interpol_n_check,
                                                 'dxinv_intpl': dxinv_intpl,
                                                 'f_lowest_intpl': 1./(len(xinv_values_intpl) * dxinv_intpl),
                                                 'f_Nyquist': 1./dxinv_intpl,
                                                 'B_LK': 2./(xinv_values_intpl[-1] + xinv_values_intpl[0]),
                                                 'zeropad_state': zeropad_bool,
                                                 'mem_state': mem_bool
                                                 }
                                                )

                # interpolate data
                # NOTE: retrieved interpolated data in dictionary with string indicating derivative order
                Yinv_intpl: dict[str, Optional[np.ndarray]] = interpolate_data(x=xinv_sel,
                                                                               y=yinv_sel,
                                                                               x_values=xinv_values_intpl,
                                                                               calc=CFlow_calculate_deriv_orders,
                                                                               exclude=CFlow_exclude_deriv_orders,
                                                                               method=interpol_method,
                                                                               )

                # check whether user wants to zero pad and total number of points has not been initialized, yet
                # NOTE: actual zero padding takes place inside if-structure of each spectral analysis method
                if zeropad_bool and (zeropad_n_total is None):

                    # set total number of points data has to have after zero padding
                    # NOTE: only in first loop of the whole run time
                    zeropad_n_total = number_of_points_in_total(N=len(xinv_values_intpl),
                                                                base2=zeropad_base2_bool,
                                                                sys_ioe=sys_stdout,
                                                                power_base2=zeropad_power_base2,
                                                                )

                    # set zeropad_bool to False so that the total number of points, defined in locals(), is the same
                    # for the rest of the run time
                    zeropad_bool = False

                # elif: user does not want to zero pad and total number of points has not been initialized, yet
                elif not zeropad_bool and (zeropad_n_total is None):

                    # setting total number of points to 0
                    # NOTE: this does not mean that data array has length 0 but that no zeros will be available for zero
                    #       padding
                    zeropad_n_total = 0

                # get info on zero padding
                # NOTE: doc_zeropad_n_total is only used for documenting the zero padding settings properly
                if not zeropad_n_total:
                    doc_zeropad_n_total: int = len(xinv_values_intpl)
                else:
                    doc_zeropad_n_total = int(zeropad_n_total)
                file_range_specific_info.update({'y_len_before_zp': len(xinv_values_intpl),
                                                 'y_len_after_zp': doc_zeropad_n_total,
                                                 'n_zeros': doc_zeropad_n_total - len(xinv_values_intpl),
                                                 'n_zeros_before': zeropad_n_before,
                                                 'n_zeros_after': (doc_zeropad_n_total - len(xinv_values_intpl) -
                                                                   zeropad_n_before),
                                                 'f_lowest_zp': 1./(dxinv_intpl * doc_zeropad_n_total),
                                                 }
                                                )

                # start looping over derivative orders for further analysis
                for deriv_order, y in Yinv_intpl.items():

                    # skip exclude derivative orders
                    if y is None:
                        continue

                    # get info on derivative order
                    file_range_specific_info.update({deriv_order: deriv_order})

                    # initialize frequency and amplitude np.ndarrays
                    #
                    # https://stackoverflow.com/...
                    # .../questions/15879315/what-is-the-difference-between-ndarray-and-array-in-numpy
                    # .../questions/10121926/initialise-numpy-array-of-unknown-length
                    freq: np.ndarray = np.array([])
                    ampl: np.ndarray = np.array([])

                    # enter FFT spectral analysis part if the core base name of the main save directory is 'SA_FFT'
                    if fft_bool and (re.sub(r'[0-9]+', '', os.path.basename(output_main_save_dir)) == 'SA_FFT'):
                        # https://stackoverflow.com/questions/12851791/removing-numbers-from-string

                        # get window function
                        window_function: np.ndarray = get_window_function(window_settings,
                                                                          N=len(y),
                                                                          fftbins=window_periodic
                                                                          )

                        # apply window function
                        y = y * window_function

                        # zero pad (default for after is 0)
                        y = zero_padding(y,
                                         n_total=zeropad_n_total,
                                         n_zeros_before=zeropad_n_before,
                                         )

                        # compute FFT spectrum
                        freq, ampl = spectrum_fft(y,
                                                  dxinv_intpl,
                                                  onesided=fft_onesided
                                                  )

                    # enter MEM spectral analysis part if the core base name of the main save directory is 'SA_MEM'
                    elif mem_bool and (re.sub(r'[0-9]+', '', os.path.basename(output_main_save_dir)) == 'SA_MEM'):

                        # zero pad (default for after is 0)
                        y = zero_padding(y,
                                         n_total=zeropad_n_total,
                                         n_zeros_before=zeropad_n_before,
                                         )

                        # -------------------------------------------------------------------------
                        #
                        #    DO NOT ALTER STUFF IN THIS '--' REGION
                        #
                        # define a text stream that saves the sys.stderr as memspectrum.py outputs information on
                        # the recursive order 'm' in this way
                        recursive_order_log_stream_stderr = io.StringIO()

                        # tee sys.stderr
                        sys.stderr = Tee(user_ioe=recursive_order_log_stream_stderr,  # type: ignore
                                         sys_active=sys_ioe_active,  # type: ignore
                                         sys_ioe=sys.stdout  # type: ignore
                                         )

                        #
                        #
                        #
                        # -------------------------------------------------------------------------

                        # compute MEM spectrum
                        freq, ampl = spectrum_mem(y,
                                                  m_order=mem_recur_order_init,
                                                  opt_method=mem_opt_method,
                                                  Burg_method=mem_Burg_method,
                                                  regularis=mem_Tikh_reg,
                                                  early_stop=mem_early_stop,
                                                  verbose=mem_verbose,
                                                  dt=dxinv_intpl,
                                                  onesided=mem_onesided
                                                  )

                        # -------------------------------------------------------------------------
                        #
                        #    DO NOT ALTER STUFF IN THIS '--' REGION
                        #
                        # get contents of recursive order log stream
                        recursive_order_log_contents: str = recursive_order_log_stream_stderr.getvalue()

                        # only need info on last iteration
                        # NOTE: for more details look at source code in 'memspectrum.py'
                        last_iteration: str = [m.strip() for m in recursive_order_log_contents.split('\r\t') if m
                                               ][-1]

                        # save values of recursive order used in MEM spectral analysis, as well as max. value of it
                        mem_recur_order_used: int = int(last_iteration.split(' ')[1])
                        mem_recur_order_max: int = int(last_iteration.split(' ')[-1])

                        # get info on recursive order used for mem
                        file_range_specific_info.update({f'{deriv_order}_m_init': mem_recur_order_init,
                                                         f'{deriv_order}_m_max': mem_recur_order_max,
                                                         f'{deriv_order}_m_used': mem_recur_order_used
                                                         }
                                                        )

                        print(f'\t\t\tMESA: {last_iteration}\n')

                        # restore original sys.stderr
                        sys.stderr = sys_stderr

                        #
                        #
                        #
                        # -------------------------------------------------------------------------

                    print('\t\t\tSaving spectral analysis output\n')

                    # save output in same format as the input files have
                    save_spectrum(freq=freq,
                                  ampl=ampl,
                                  invfield_range=inv_field_rng,
                                  main_save_dir=output_main_save_dir,
                                  sub_save_dir=output_sub_save_dir,
                                  in_file_info=Library_input_file_info[input_fbasename],
                                  derivative_order=deriv_order,
                                  cutoff=output_freq_cutoff,
                                  normalize_by_max=output_norm_by_max,
                                  normalize_by_mean=output_norm_by_mean,
                                  multiple_y_columns=output_multiple_y_cols,
                                  ext=output_file_ext,
                                  comment_floatformat=output_comment_floatformat
                                  )

                # update library for storing range specific settings for active input file
                Library_file_range_specific_info.update({input_fbasename: file_range_specific_info})

            # save library dictionary of range specific settings of all analyzed input files for active range
            Settings_RangeSpecific[f'r{inv_field_rng_idx + 1}'].update({f'r{inv_field_rng_idx + 1}'
                                                                        f'_Library_file_range_specific_info':
                                                                            Library_file_range_specific_info
                                                                        }
                                                                       )

        print(f'Finished spectral analysis for {os.path.basename(output_main_save_dir)}\n')

        # -------------------------------------------------------------------------
        #
        #
        #
        ##############################################
        # READ/WRITE GENERAL & SPECTRAL LOG STREAMS  #
        ##############################################

        combine_and_write_to_log(general_log_stream_stdout,
                                 spectral_log_stream_stdout,
                                 dir_path=output_main_save_dir,
                                 mem_active=mem_bool)

        #
        #
        #
        # -------------------------------------------------------------------------

        # -------------------------------------------------------------------------
        #
        #
        #
        ####################################################################
        # UPDATE RANGE SPECIFIC DOCUMENTATION for SPECTRAL ANALYSIS METHOD #
        ####################################################################

        # update documentation for all range specific settings
        documentation_update_settings_range_specific(documentation=Documentation_wb,
                                                     settings_rangespecific=Settings_RangeSpecific
                                                     )

        #
        #
        #
        # -------------------------------------------------------------------------

        # -------------------------------------------------------------------------
        #
        #
        #
        ###################################################################
        # EDIT & SAVE COMPLETE DOCUMENTATION for SPECTRAL ANALYSIS METHOD #
        ###################################################################

        # edit layout of documentation
        documentation_edit_layout(documentation=Documentation_wb)

        # save documentation
        Documentation_wb.save(filename=output_main_save_dir + os.sep + os.path.basename(output_main_save_dir) +
                              '_documentation.xlsx'
                              )

        #
        #
        #
        # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #
    #
    #
    ###############################
    # RESTORE DEFAULT I/O STREAM  #
    ###############################

    # restore original sys.stdout
    sys.stdout = sys_stdout

    #
    #
    #
    # -------------------------------------------------------------------------

    print('Finished spectral analysis')

    return 0
# ____________________________________________________________________________________________________________________ #

# ____________________________________________________________________________________________________________________ #


if __name__ == '__main__':

    # get absolute path to json file (stores all user input) from parsed argument
    json_fp = parsing().json_filepath

    # get boolean indicating whether standard system I/O rerouting will be disabled
    sys_ioe_flag = parsing().system_ioe

    # load content from json file into a dictionary
    with open(json_fp) as json_content:
        parameters_user = json.load(json_content)

    # call main()
    main(parameters=parameters_user,
         sys_ioe_active=sys_ioe_flag
         )
