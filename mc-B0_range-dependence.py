import argparse as argp

import matplotlib as mpl  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import matplotlib.ticker as tck

import itertools

import json

import glob  # type: ignore
import numpy as np
import operator
import os
import re
import sys

from typing import Any, Match, Optional

from spectral_analysis import create_file_info
from spectral_analysis import get_file_basenames
from spectral_analysis import load_data
from spectral_analysis import measurement_control_variable_value
from spectral_analysis import validate_filepath


def get_directories(parent_dir: str) -> list[str]:
    """
    function returning list of absolute paths that stand for each child directory present in the given parent directory

    :param parent_dir: string indicating the absolute path of the parent directory
    :return: list of strings with each string being the absolute path of each child directory in the given parent
             directory
    """
    # check that a parent directory is given
    if not parent_dir:
        print('ERROR: cannot give child directories for no specified parent directory!')
        sys.exit(1)

    # return list of child directories
    # https://stackoverflow.com/questions/800197/how-to-get-all-of-the-immediate-subdirectories-in-python
    return [parent_dir + os.sep + name for name in os.listdir(parent_dir)
            if os.path.isdir(os.path.join(parent_dir, name))
            ]


def numsort_dirs(dir_path: str) -> int:
    """
    function that returns the unique integer identifying the input directory

    :param dir_path: string indicating the absolute path of the input directory
    :return: integer that uniquely identifies the input directory
    """

    # get the base name of the input directory
    # NOTE: input directory base names should consist out of capital or lower letters only!
    dir_basename: str = os.path.basename(dir_path)

    # define pattern for regex expression search
    string_pattern: str = r'([A-Z]+(_)?[A-Z]+|[a-z])'
    string_match: Optional[Match[str]] = re.search(string_pattern, dir_basename)
    assert string_match is not None

    # get the uniquely identifying integer by replacing the letter part of the input directory base name with nothing
    dir_basename_no = int(dir_basename.replace(string_match.group(), ''))

    # return uniquely identifying integer
    return dir_basename_no


def parsing():
    """
    function handling argument parsing

    :return: Namespace containing all parsed arguments
    """

    # https://stackoverflow.com/questions/59178961/how-to-get-file-path-from-user-to-then-pass-into-functions-argparse
    # https://www.golinuxcloud.com/python-argparse/
    # https://docs.python.org/dev/library/argparse.html#the-add-argument-method

    # create parser
    parser = argp.ArgumentParser()

    # add file path as positional argument
    parser.add_argument('json_filepath',
                        help='Absolute path to .json file where all input parameters are declared',
                        type=validate_filepath,
                        metavar='JSON FILEPATH'
                        )
    # return arguments
    return parser.parse_args()


def main(settings: dict[str, Any]) -> int:

    # _________________________________________________
    # get settings from json file
    input_method: str = settings['input_method']  # original, MCV, range, compare_SA

    input_SA_range: Optional[str] = settings['input_SA_range']

    input_SA_dirs: Optional[list[str]] = settings['input_SA_dirs']

    input_main_dir: str = settings['input_main_directory']

    input_derivative: str = settings['input_derivative']

    input_files_to_plot: list[str] = settings['input_files_to_plot']
    input_reverse: bool = settings['input_reverse']

    input_delim: str = settings['input_delim']
    input_file_ext: str = settings['input_file_ext']
    input_skiprows: int = settings['input_skiprows']
    input_use_columns: tuple[int, int] = (settings['input_use_columns_x'],
                                          settings['input_use_columns_y']
                                          )
    input_var_names_line_no: Optional[int] = settings['input_var_names_line_no']

    input_MCV_name: str = settings['input_MCV_name']
    input_MCV_units: str = settings['input_MCV_units']

    input_subtract_first_value: bool = settings['input_subtract_first_value']
    input_normalize_by_mean: bool = settings['input_normalize_by_mean']

    plot_cmap_name: str = settings['plot_cmap_name']
    plot_cmap_tuner: float = settings['plot_cmap_tuner']
    plot_rcParams: dict[str, Any] = settings['plot_rcParams']
    plot_ticks_dir: str = settings['plot_ticks_dir']
    plot_legend_linewidth: int = settings['plot_legend_linewidth']
    plot_legend_draggable: bool = settings['plot_legend_draggable']
    plot_legend_ncol: int = settings['plot_legend_ncol']
    plot_xlb: Optional[float] = settings['plot_xlb']
    plot_xub: Optional[float] = settings['plot_xub']
    plot_ylb: Optional[float] = settings['plot_ylb']
    plot_yub: Optional[float] = settings['plot_yub']
    plot_offset: Optional[float] = settings['plot_offset']
    plot_cos_scale: bool = settings['plot_cos_scale']
    plot_only_theory: bool = settings['plot_only_theory']
    plot_scatter: bool = settings['plot_scatter']
    plot_scatter_size: float = settings['plot_scatter_size']
    plot_scatter_vmin_threshold: float = settings['plot_scatter_vmin_threshold']
    plot_scatter_vmax_scaled: float = settings['plot_scatter_vmax_scaled']
    plot_scatter_marker: str = settings['plot_scatter_marker']
    plot_scatter_alpha: float = settings['plot_scatter_alpha']
    plot_scatter_theory: bool = settings['plot_scatter_theory']
    plot_scatter_DFTTony: bool = settings['plot_scatter_DFTTony']
    plot_scatter_DFT_ls_fpaths: list[str] = settings['plot_scatter_DFT_ls_fpaths']
    plot_scatter_DFT_ls_colors: list[str] = settings['plot_scatter_DFT_ls_colors']

    export_active: bool = settings['export_active']
    export_dir: str = settings['export_dir']
    export_only: bool = settings['export_only']

    # _________________________________________________
    # set search term
    search_term = 'FitXcld'

    # _________________________________________________
    # set sample name and frequency
    sample = 'ZrSiSe'
    freq = '209T'

    # _________________________________________________
    # set plot parameters
    for k, v in plot_rcParams.items():
        if v is not None:
            plt.rcParams[k] = v

    # _________________________________________________
    # get the right input directory
    input_data_dir: str = input_main_dir
    if input_method == 'ranges':
        ranges = [os.path.basename(range_dir) for range_dir in sorted(get_directories(input_data_dir),
                                                                      key=numsort_dirs)]

    # _________________________________________________
    # get all possible data files
    input_fbasenames_all: list[str] = []

    if input_method == 'ranges':
        for rng in ranges:
            input_temp_fnames = []
            input_temp_fnames += get_file_basenames(rsrc_dir=input_data_dir + os.sep + rng + os.sep +
                                                             os.path.basename(input_data_dir) + '_' + rng + '_peaks',
                                                    ext='txt'
                                                    )
            for i in range(0, len(input_temp_fnames)):
                input_temp_fnames[i] = rng + os.sep + os.path.basename(input_data_dir) + '_' + rng + '_peaks' + os.sep +\
                                       input_temp_fnames[i]
            input_fbasenames_all += input_temp_fnames

    input_fbasenames_all = [elem for elem in input_fbasenames_all if search_term in elem]


    Bavg_mc_pm_err = {}
    for fname in input_fbasenames_all:
        with open(input_data_dir + os.sep + fname, 'r') as f:
            for _ in range(13):
                next(f)
            mc_line = f.readline().split(' ')
            mc = mc_line[9]
            err = mc_line[11]
            B_avg = f.readline().split(' ')[6]
        Bavg_mc_pm_err.update({B_avg: (mc, err)})


    fig = plt.figure()
    ax = fig.add_subplot()
    x = []
    y = []
    yerr = []
    for k in Bavg_mc_pm_err.keys():
        x.append(float(k))
        y.append(float(Bavg_mc_pm_err[k][0]))
        yerr.append(float(Bavg_mc_pm_err[k][1]))

    x = np.asarray(x)
    y = np.asarray(y)
    yerr = np.asarray(yerr)
    ax.errorbar(x, y, yerr=yerr, fmt='o', markersize=4)

    # set plot boundaries
    ax.set_aspect('auto')
    ax.set_xbound(lower=plot_xlb, upper=plot_xub)
    ax.set_ybound(lower=plot_ylb, upper=plot_yub)

    # set labels
    ax.set_xlabel('$B_{avg}$ (T)')
    ax.set_ylabel('$m_{c}$ ($m_{0})$')

    # set title
    ax.set_title(sample+': '+freq)

    plt.show()


    return 0


if __name__ == '__main__':
    # get absolute path to json file (stores plotting settings) from parsed argument
    json_fp = parsing().json_filepath

    # load content from json file into a dictionary
    with open(json_fp) as json_content:
        settings_plotting = json.load(json_content)

    # call main()
    main(settings=settings_plotting)
